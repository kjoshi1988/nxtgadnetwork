# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table project_item_data (
  id                        varchar(255) not null,
  body_css                  varchar(2048),
  header_css                varchar(2048),
  logo_css                  varchar(2048),
  search_css                varchar(2048),
  categories_css            varchar(2048),
  nla_css                   varchar(2048),
  product_css               varchar(2048),
  p_list_css                varchar(2048),
  related_search_css        varchar(2048),
  footer_css                varchar(2048),
  constraint pk_project_item_data primary key (id))
;

create table project_items (
  id                        varchar(255) not null,
  title                     varchar(255),
  owner                     varchar(255),
  about_us                  varchar(255),
  channel_name              varchar(255),
  twitter_page              varchar(255),
  fb_page                   varchar(255),
  g_plus_page               varchar(255),
  sprite_url                varchar(255),
  constraint pk_project_items primary key (id))
;

create sequence project_item_data_seq;

create sequence project_items_seq;




# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists project_item_data;

drop table if exists project_items;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists project_item_data_seq;

drop sequence if exists project_items_seq;


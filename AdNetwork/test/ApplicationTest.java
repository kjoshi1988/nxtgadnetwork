import config.ContextConfig;
import controllers.adNetwork.util.StubData;
import models.adNetwork.adsWidget.AdDashBoard;
import models.adNetwork.adsWidget.Category;
import models.adNetwork.adsWidget.Discount;
import org.junit.Before;
import org.junit.Test;
import play.libs.Yaml;
import play.mvc.Http;
import play.twirl.api.Content;
import views.html.app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.contentType;


/**
 * Simple (JUnit) tests that can call all parts of a play app.
 * If you are interested in mocking a whole application, see the wiki for more details.
 */
public class ApplicationTest {
    private final Http.Request request = mock(Http.Request.class);
    @Before
    public void setUp() {
    }


    @Test
    public void simpleCheck() {
        int a = 1 + 1;
        assertThat(a).isNotEqualTo(3);
    }

    @Test
    public void renderTemplate() {
        mock(Yaml.class);
//        mock(Category.class);
//        mock(Discount.class);

        AdDashBoard adDashBoard = new AdDashBoard();
        adDashBoard.categories = Collections.emptyList();
        adDashBoard.discounts = Collections.emptyList();
        adDashBoard.columnCount = 5;
        adDashBoard.rowCount = 1;
        adDashBoard.defaultBgColor = "#f5f5f5";
        adDashBoard.keyword = "ipod";
        adDashBoard.priceDrop ="ipod";

        Map<String, String> flashData = Collections.emptyMap();
        Map<String, Object> argData = Collections.emptyMap();
        Long id = 2L;
        play.api.mvc.RequestHeader header = mock(play.api.mvc.RequestHeader.class);
        Http.Context context = new Http.Context(id, header, request, flashData, flashData, argData);
        Http.Context.current.set(context);
        ContextConfig.put("adDashBoard", adDashBoard);
        Content html = app.render("Your new application is ready.");
        assertThat(contentType(html)).isEqualTo("text/html");
        assertThat(contentAsString(html)).contains("Your new application is ready.");
    }




}

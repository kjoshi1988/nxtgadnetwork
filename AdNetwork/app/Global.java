import com.avaje.ebean.Ebean;
import controllers.adNetwork.util.AppConstants;
import models.silhouette.ProjectItemData;
import models.silhouette.ProjectItems;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.libs.F;
import play.libs.Yaml;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;
import views.html.error.pageNotFound;

import java.util.List;

/**
 * User: kjoshi
 * Date: 06/11/14
 * Time: 3:55 PM
 */
public class Global extends GlobalSettings {
    private final Logger.ALogger logger = Logger.of(Global.class);

    @Override
    public void onStart(Application application) {
//        System.out.println("[APP]ThriftConstants.THRIFT_HOST::"+ ThriftConstants.THRIFT_HOST);
        /**
         * Here we load the initial data into the database
         */
        if (Ebean.find(ProjectItems.class).findRowCount() == 0) {
//            Map<String,List<Object>> all = (Map<String,List<Object>>) ;
            Object o = Yaml.load("testData/silhouette/quickLauncher.yml");
            if (o != null && o instanceof List)
                Ebean.save((List<ProjectItems>) o);
        }
        if (Ebean.find(ProjectItemData.class).findRowCount() == 0) {
//            Map<String,List<Object>> all = (Map<String,List<Object>>) ;
            Object o = Yaml.load("testData/silhouette/dashBoard.yml");
            if (o != null && o instanceof List)
                Ebean.save((List<ProjectItemData>) o);
        }

//        Akka.system().scheduler().schedule(
//                Duration.create(0, TimeUnit.SECONDS),
//                Duration.create(10, TimeUnit.SECONDS),
//                new Runnable() {
//                    @Override
//                    public void run() {
//                        Map map ;
//                    }
//                },
//                Akka.system().dispatcher()
//        );
    }

//    @Override
//    public Action onRequest(Http.Request request, Method method) {
//        return new Action.Simple() {
//            public F.Promise<Result> call(Http.Context ctx) throws Throwable {
//            Http.Request request = ctx.request();
//            if (LoginUtil.isFromInternalNetwork(request)) {
//                if (logger.isDebugEnabled()) logger.debug("Is internal url");
//                return delegate.call(ctx);
//            }
//            if(LoginUtil.isForIframe(request)){
//                return delegate.call(ctx);
//            }
//            if(LoginUtil.isForPdf(request)){
//                return delegate.call(ctx);
//            }
////            WizeSession wizeSession = WizeSession.initSession(ctx.session(), ctx.response());
////            boolean isLoggingUrl = LoginUtil.isLoginUrl(request);
////            boolean isUserLoggedIn = LoginUtil.isUserLoggedIn(wizeSession);
////            if (isLoggingUrl || isUserLoggedIn || LoginUtil.isSignInAuthReq(request, wizeSession)) {
////                if (isUserLoggedIn && isLoggingUrl) {
////                    if (logger.isDebugEnabled()) logger.debug("User already logged in, redirecting to dashboard");
////                    return F.Promise.pure(redirect(AppConstants.URL.dashBoardURL));
////                }
////                if (logger.isDebugEnabled()) logger.debug("Continue to current controller");
//                return delegate.call(ctx);
////            }
////            if (logger.isDebugEnabled()) logger.debug("User not logged in, redirecting to login page");
////            return F.Promise.pure(redirect(AppConstants.URL.loginCall));
//            }
//        };
//    }

    @Override
    public F.Promise<Result> onHandlerNotFound(Http.RequestHeader requestHeader) {
        if (logger.isDebugEnabled()) logger.debug("Page not found");
        Result notFound = Results.notFound(pageNotFound.render(AppConstants.title));
        return F.Promise.pure(notFound);
    }
}

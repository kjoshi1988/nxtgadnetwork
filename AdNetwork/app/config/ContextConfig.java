package config;

import play.mvc.Http;

/**
 * User: kjoshi
 * Date: 11/11/14
 * Time: 4:23 PM
 */
public class ContextConfig{
    public static void put(String key, Object value) {
        Http.Context.current().args.put(key, value);
    }
    public static Object get(String key){
        return Http.Context.current().args.get(key);
    }
    public static Object remove(String key){
        return Http.Context.current().args.remove(key);
    }
}

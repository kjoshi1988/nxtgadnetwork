package config.util;

import controllers.adNetwork.util.AppUtil;
import play.Play;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.List;

/**
 * User: kjoshi
 * Date: 30/12/14
 * Time: 12:56 AM
 */
public class Resource {
    private static Resource resource;

    public static Resource getInstance(){
        if(resource == null)
            resource = new Resource();
        return resource;
    }

    public String getResourceUrl(String resourceKey, String resourceType){
        List<String> commonList = AppUtil.appConfig().getStringList("resources." + resourceKey + "." + resourceType + ".common");
        String url="";
        return url;
    }

    private MessageDigest getMd5Hash(String resource, MessageDigest md) throws IOException {
        InputStream in = null;
        DigestInputStream dis = null;
        byte[] buffer = new byte[8192];
            in = Play.class.getResourceAsStream(resource);
        if (in == null)
            throw new FileNotFoundException("Resource not found: " + resource);
        dis = new DigestInputStream(in, md);
        try {
            while (dis.read(buffer) != -1) {}
        } catch (IOException e) {
            throw new IOException(resource + ": " + e.getMessage(), e);
        } finally {
            try {
                dis.close();
            } catch (IOException ee) {}
            try {
                in.close();
            } catch (IOException ee) {}
        }

        return md;
    }
}

package config.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * User: kjoshi
 * Date: 06/01/15
 * Time: 7:21 PM
 */
public class UrlWrapper {
    private static final String enc = "UTF-8";
    private static final String PREFIX = "/wizeAds/clickout?";
    private static final String CLICK_TRACK_PARAM_NAME = "clickTrackUrl";
    private static final String NT_DOMAIN = "http://www.nextag.com";

    public static String encode(String clickTrackUrl){
        try {
            return PREFIX + CLICK_TRACK_PARAM_NAME + "=" + URLEncoder.encode(clickTrackUrl, enc);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return PREFIX;
    }

    public static String decode(String clickTrackUrl){
        return NT_DOMAIN + clickTrackUrl;
    }
}

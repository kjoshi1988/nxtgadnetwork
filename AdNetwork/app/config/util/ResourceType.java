package config.util;

/**
 * User: kjoshi
 * Date: 30/12/14
 * Time: 1:08 AM
 */
public enum ResourceType {
    JS("js"),
    CSS("css");

    String value;

    private ResourceType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

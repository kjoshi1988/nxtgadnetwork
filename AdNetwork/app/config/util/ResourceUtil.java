package config.util;

import java.io.*;
import java.util.Scanner;

/**
 * User: kjoshi
 * Date: 03/01/15
 * Time: 10:16 PM
 */
public class ResourceUtil {
    public static String readFromFile(String filePath) {
        return readFromFile(new File(filePath));
    }

    public static String readFromFile(InputStream inputStream) {
        StringBuilder content = new StringBuilder();
        try {
            Scanner scanner = new Scanner(inputStream);
            scanner.useDelimiter("\\Z");
            content.append(scanner.next());
        } finally {
            try {
                if (inputStream != null)
                    inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return content.toString();
    }

    public static String readFromFile(File file) {
        StringBuilder content = new StringBuilder();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            Scanner scanner = new Scanner(fis);
            scanner.useDelimiter("\\Z");
            content.append(scanner.next());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null)
                    fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return content.toString();
    }
}

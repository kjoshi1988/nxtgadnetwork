package config.util;

/**
 * User: kjoshi
 * Date: 30/12/14
 * Time: 1:30 AM
 */
public enum ResourceKey {
    Silhouette("silhouette"),
    AdNtwrk("adNtwrk");
    String value;

    private ResourceKey(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

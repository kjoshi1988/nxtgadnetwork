package config.util;

import com.wizecommerce.adnetwork.common.Product;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

/**
 * User: kjoshi
 * Date: 06/01/15
 * Time: 4:03 PM
 */
public class JSONUtil {
    public static JSONObject productToJson(Product product) {
        JSONObject jsonObject = new JSONObject();
        if(product != null){
            jsonObject.put("name", product.getName());
            jsonObject.put("price", product.getPrice());
            jsonObject.put("imgSrc", product.getImgSrc());
            jsonObject.put("sellerDispTxt", product.getSeller());
            jsonObject.put("clickUrl", StringUtils.isNotBlank(product.getClickURL()) ? product.getClickURL() : "");
        }
        return jsonObject;
    }
}

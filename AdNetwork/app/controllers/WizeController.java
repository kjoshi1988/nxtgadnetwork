package controllers;

import controllers.session.WizeSession;
import play.mvc.Controller;

/**
 * User: kjoshi
 * Date: 02/12/14
 * Time: 12:40 PM
 */
@WizeSession.Init.SessionInitializer
public abstract class WizeController extends Controller{
}

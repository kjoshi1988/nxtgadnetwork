package controllers.session;

import config.ContextConfig;
import controllers.adNetwork.util.AppUtil;
import play.Logger;
import play.cache.Cache;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * User: kjoshi
 * Date: 24/11/14
 * Time: 4:10 PM
 */
public class WizeSession {
    private String id;
    private long lastAccessTime;
    private Http.Context ctx;
    private Map<String, Object> sessionMap;
    private static final Logger.ALogger logger = Logger.of(WizeSession.class);

    private WizeSession() {
    }

    private static WizeSession initSession(Http.Context ctx) {
        Http.Session session = ctx.session();
        String currentUUID = session.get("uuid");
        if (logger.isDebugEnabled())
            logger.debug("UUID in cookie:" + currentUUID);
        WizeSession wizeSession = (WizeSession) Cache.get(currentUUID);
        if (wizeSession == null) {
            if (logger.isDebugEnabled())
                logger.debug("WizeSession not found in cache for:" + currentUUID);
            wizeSession = createNewSession(ctx);
        } else {
            long sessionIdleTimeout = AppUtil.getSessionIdleTimeoutInSeconds();
            long currentTime = new Date().getTime();
            if (logger.isDebugEnabled()) {
                logger.debug("WizeSession found in cache for:" + currentUUID);
                logger.debug("SessionIdleTimeout:" + sessionIdleTimeout + "(s)");
                logger.debug("Idle time:" + (currentTime - wizeSession.lastAccessTime) + "(ms)");
            }
            if ((currentTime - wizeSession.lastAccessTime) / 1000 > sessionIdleTimeout) {
                wizeSession.invalidate();
                createNewSession(ctx);
            } else
                wizeSession.lastAccessTime = currentTime;
        }
        ContextConfig.put("_session", wizeSession);
        return wizeSession;
    }

    public static WizeSession getCurrentSession(Http.Session session) {
        String uuid = session.get("uuid");
        return uuid == null ? null : (WizeSession) Cache.get(uuid);
    }

    public Http.Session session(){
        return ctx == null ? null : ctx.session();
    }

    public Http.Request request(){
        return ctx == null ? null : ctx.request();
    }

    public Http.Response response(){
        return ctx == null ? null : ctx.response();
    }

    public String getId() {
        return id;
    }

    public Object get(String key) {
        return sessionMap.get(key);
    }

    public boolean getBoolean(String key) {
        return Boolean.parseBoolean(sessionMap.get(key) + "");
    }

    public void put(String key, Object value) {
        sessionMap.put(key, value);
    }

    public void invalidate() {
        invalidate(false);
    }

    public void invalidate(boolean createNewSession) {
        sessionMap.clear();
        if(createNewSession){
            Cache.remove(id);
            session().clear();
        } else
         lastAccessTime = new Date().getTime();
    }

    private static WizeSession createNewSession(Http.Context context) {
        String uuid = java.util.UUID.randomUUID().toString();
        WizeSession wizeSession = new WizeSession();
        wizeSession.id = uuid;
        wizeSession.sessionMap = new HashMap<String, Object>();
        wizeSession.ctx = context;
        wizeSession.lastAccessTime = new Date().getTime();

        context.session().clear();
        int sessionMaxAge = AppUtil.getSessionMaxAgeInSeconds();
        context.session().put("uuid", uuid);
        Cache.set(uuid, wizeSession, sessionMaxAge);
        context.response().setHeader(Http.HeaderNames.CACHE_CONTROL, "max-age=" + sessionMaxAge);
        return wizeSession;
    }

    public static class Init extends Action<Init.SessionInitializer> {
        @Override
        public F.Promise<Result> call(Http.Context ctx) throws Throwable {
            WizeSession.initSession(ctx);
            return delegate.call(ctx);
        }

        @With(Init.class)
        @Target({ElementType.TYPE, ElementType.METHOD})
        @Retention(RetentionPolicy.RUNTIME)
        public static @interface SessionInitializer {
            java.lang.Class<? extends Init> value() default Init.class;
        }
    }

}

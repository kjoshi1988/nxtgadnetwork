package controllers.performance;

import controllers.adNetwork.util.AppConstants;
import net.sf.ehcache.CacheManager;
import play.libs.F;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.performance.cache;


/**
 * User: kjoshi
 * Date: 25/11/14
 * Time: 3:53 PM
 */
public class CacheController extends Controller{
    public static F.Promise<Result> viewAll(){
        CacheManager.getInstance().getEhcache("play").getKeys();
        for(Object o: CacheManager.getInstance().getEhcache("play").getKeys()){
            System.out.println("Names:"+o);
        }
        return F.Promise.pure((Result)ok(cache.render(AppConstants.title)));
    }
    public static F.Promise<Result> view(String id){
        return F.Promise.pure((Result)ok("all"));
    }
}

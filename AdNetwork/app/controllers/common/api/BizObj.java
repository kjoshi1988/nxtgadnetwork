package controllers.common.api;

import com.adNetwork.service.util.BizObjServiceImpl;
import com.wizecommerce.adnetwork.common.INode;
import config.util.ResourceUtil;
import org.json.JSONObject;
import play.Play;
import play.libs.F;
import play.mvc.Controller;
import play.mvc.Result;

import java.io.File;
import java.text.MessageFormat;
import java.util.Map;
import java.util.Set;


/**
 * User: kjoshi
 * Date: 02/01/15
 * Time: 7:13 PM
 */
public class BizObj extends Controller {

    public static F.Promise<Result> loadCategories() {
        return F.Promise.pure(fetchCategories());
    }

    private static Result fetchCategories() {
        /*Map<Long, INode> categories = BizObjServiceImpl.instance().fetchCategories((short) 1);
        INode root = categories == null  ? null : categories.get(-1L);*/
        response().setContentType("application/json");
        /*if (root != null && root.getChildren() != null && !root.getChildren().isEmpty()) {
            return ok(fetchChildren(root.getChildren(), categories).toString());
        }*/
        return ok(ResourceUtil.readFromFile(Play.application().path().getAbsolutePath() + "/conf/testData/defaultCategories.json"));
    }

    public static F.Promise<Result> wizeAdsScript(){
        return F.Promise.promise(new F.Function0<Result>() {
            @Override
            public Result apply() throws Throwable {
                String scriptSrc = ResourceUtil.readFromFile(Play.application().classloader().getResourceAsStream("public/javascripts/api/wizeAds.js"));
                return ok(scriptSrc.replace("{0}", request().getHeader("host"))).as("text/javascript");
            }
        });
    }

    private static JSONObject getNodeAsJson(INode node) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", node.getId());
        jsonObject.put("name", node.getName());
        jsonObject.put("parent", node.getParent());

        return jsonObject;
    }

    private static JSONObject fetchChildren(Set<Long> children, Map<Long, INode> categoryMap) {
        JSONObject childrenObj = new JSONObject();
        for (long id : children) {
            INode node = categoryMap.get(id);
            if (node != null) {
                JSONObject nodeJson = getNodeAsJson(node);
                if (node.getChildren() != null && !node.getChildren().isEmpty())
                    nodeJson.put("children", fetchChildren(node.getChildren(), categoryMap));
                childrenObj.put(String.valueOf(node.getId()), nodeJson);
            }
        }
        return childrenObj;
    }

}

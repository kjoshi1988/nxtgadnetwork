package controllers.silhouette;

import controllers.WizeController;
import controllers.silhouette.util.SilhouetteConstants;
import models.silhouette.ProjectItems;
import play.data.Form;
import play.libs.Json;
import play.libs.Yaml;
import play.mvc.Result;
import views.html.silhouette.app;

/**
 * User: kjoshi
 * Date: 22/12/14
 * Time: 4:30 PM
 */
public class App extends WizeController {
    public static Result index() {
        return ok(app.render(SilhouetteConstants.title));
    }

    public static Result listOfProjects() {
        return ok(Json.toJson(ProjectItems.findAll()));
    }

    public static Result createNewProject(){
        ProjectItems projectItems = new ProjectItems();
        projectItems.title = Form.form().bindFromRequest().get("name");
        projectItems.owner = Form.form().bindFromRequest().get("owner");
        projectItems.aboutUs = Form.form().bindFromRequest().get("aboutUs");
        projectItems.spriteUrl = Form.form().bindFromRequest().get("spriteUrl");
        projectItems.gPlusPage = Form.form().bindFromRequest().get("gPlusPage");
        projectItems.fbPage = Form.form().bindFromRequest().get("fbPage");
        projectItems.twitterPage = Form.form().bindFromRequest().get("twitterPage");
        projectItems.channelName = Form.form().bindFromRequest().get("channelName");
        projectItems.id = (ProjectItems.findAll().size() + 1 )+"";
        projectItems.save();
        return ok(Json.toJson(projectItems));
    }

    private void save(){

    }
}

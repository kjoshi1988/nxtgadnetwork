package controllers.silhouette.dashboard;

import com.adNetwork.service.util.BizObjServiceImpl;
import com.wizecommerce.adnetwork.common.INode;
import config.ContextConfig;
import controllers.WizeController;
import controllers.session.WizeSession;
import controllers.silhouette.util.SilhouetteConstants;
import models.silhouette.ProjectItemData;
import models.silhouette.ProjectItems;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import play.libs.Json;
import play.libs.Yaml;
import play.mvc.Result;
import views.html.silhouette.dashboard.htmlEditor;
import views.html.silhouette.silhouetteDashboardContent;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;

public class SilhouetteDashboardController extends WizeController {
    public static Result getSilhouetteDashboard(long projectId) {
        ContextConfig.put("sideNavItems", Yaml.load("Silhouette/leftNavItems.yml"));
        WizeSession.getCurrentSession(session()).put("projectId", projectId);
        ProjectItemData projectItemData = ProjectItemData.find.byId(String.valueOf(projectId));
        if (projectItemData == null) {
            projectItemData = ProjectItemData.loadDefault(String.valueOf(projectId));
            projectItemData.save();
        }
        return ok(silhouetteDashboardContent.render(SilhouetteConstants.title, String.valueOf(projectId)));
    }

    public static Result loadProjectData(String projectid) {
        ProjectItemData projectItemData = new ProjectItemData();
        return ok(Json.toJson(projectItemData.find.byId(projectid)));
    }

    public static Result loadHtmlEditor(String projectId) {
        ProjectItems projectItem = ProjectItems.find.byId(String.valueOf(projectId));
        ContextConfig.put("projectTitle", projectItem == null ? "" : projectItem.title);
        return ok(htmlEditor.render());
    }
    public static Result saveProjectData(String projectId){
    	ProjectItemData projectItem = ProjectItemData.find.byId(String.valueOf(projectId));
    	JsonNode jsonObj = request().body().asJson();
    	projectItem = Json.fromJson(jsonObj, ProjectItemData.class);
    	System.out.println(jsonObj);
    	System.out.println(projectItem);
    	projectItem.update();
    	return ok();
    }

    public static Result getProperties(String projectId) {
        String output = "";
        try {
            Properties prop = new Properties();
            StringBuilder outputProp = new StringBuilder();

            String filePath = System.getProperty("user.dir") + "/conf/testData/silhouette/template.tmp";
            FileInputStream fis = new FileInputStream(new File(filePath));
            ProjectItemData projectItemData = ProjectItemData.find.byId(projectId);
            ProjectItems projectItems = ProjectItems.find.byId(projectId);
            if(projectItemData!=null){
                prop.load(fis);
                Enumeration propNames = prop.propertyNames();
                JSONObject p_list = new JSONObject(projectItemData.p_listCss);
                JSONObject productCss = new JSONObject(projectItemData.productCss);
                JSONObject bodyCss = new JSONObject(projectItemData.bodyCss);
                JSONObject headerCss = new JSONObject(projectItemData.headerCss);
                JSONObject logoCss = new JSONObject(projectItemData.logoCss);
                JSONObject searchCss = new JSONObject(projectItemData.searchCss);
                JSONObject catCss = new JSONObject(projectItemData.categoriesCss);
                JSONObject nlaCss = new JSONObject(projectItemData.nlaCss);
                JSONObject footerCss = new JSONObject(projectItemData.footerCss);


                StringBuilder css = new StringBuilder();

                //bodyCss
                css.append("body{").append(bodyCss.getString("Background CSS")).append("}");
                //headerCss
                css.append(".mainDiv{").append(headerCss.getString("Background CSS")).append("}");
                //logoCss
                css.append(".logoWrap a.logoWrap_a{").append(logoCss.getString("CSS")).append("}");
                //searchCss
                css.append(".searchBoxWrap div.searchBox{").append(searchCss.getString("Search box")).append("}");
                css.append(".searchFrm div.textWrap{").append(searchCss.getString("Box wrap css")).append("}");
                css.append("#searchInput{").append(searchCss.getString("Box css")).append("}");
                css.append(".searchFrm div.submtImgWrap{").append(searchCss.getString("Button wrap css")).append("}");
                css.append(".submtImgWrap .submtImg{").append(searchCss.getString("Button css")).append("}");

                //categoryCss
                css.append(".mainDiv div.headerContainer {").append(catCss.getString("Wrap css")).append("}");
                css.append(".menuBar div.menuRootNode {").append(catCss.getString("Node block css")).append("}");
                css.append(".menuRootNode a.shineAnchor {").append(catCss.getString("Node css")).append("}");
                css.append("ul.subMenu_ul {").append(catCss.getString("Submenu container")).append("}");
                css.append(".subMenu_ul li.subMenu_li {").append(catCss.getString("Submenu item")).append("}");
                css.append(".subMenu_ul li.subMenu_li:hover  {").append(catCss.getString("Submenu item hover")).append("}");
                css.append(".subMenu_li a.subMenu_a {").append(catCss.getString("Submenu link")).append("}");
                css.append(".subMenu_li a.subMenu_a:hover {").append(catCss.getString("Submenu link hover")).append("}");

                //nlaCss
                css.append(".search_results_content_class div.nlaWrap{").append(nlaCss.getString("Wrap css")).append("}");
                css.append(".nlaWrap .floatLeft#filterText {").append(nlaCss.getString("Refine text css")).append("}");
//                css.append(".matchesString#resultsMessage{").append(nlaCss.getString("Pagination info css")).append("}");
                css.append(".nlaContainer ul.nlaList{").append(nlaCss.getString("Sub nla container")).append("}");
                css.append(".nlaContainer .nlaList li.nlaRow-level-0{").append(nlaCss.getString("Sub nla item")).append("}");
                css.append(".nlaContainer .nlaList li.nlaRow-level-0:hover{").append(nlaCss.getString("Sub nla item hover")).append("}");

                //product css
//                css.append(".li_float.row_basic_style.row_li {").append(p_list.getString("Product Container Css")).append("}");

                //product block css
                css.append(".imgContainer {").append(productCss.getString("Image css")).append("}");
                css.append(".titleName {").append(productCss.getString("Title css")).append("}");
                css.append("div.price-range-grid{").append(productCss.getString("Price css")).append("}");
                css.append(".seller-info-grid b {").append(productCss.getString("Seller name css")).append("}");
                css.append(".wizeRating.blankStar {").append(productCss.getString("Rating blank star css")).append("}");
                css.append(".wizeRating.fullStar{").append(productCss.getString("Rating full star css")).append("}");
                css.append(".wizeRating.halfStar{").append(productCss.getString("Rating half star css")).append("}");

                //footer css
                css.append(".footerLinkWrap {").append(footerCss.getString("Nav css")).append("}");
                css.append(".disclaimer {").append(footerCss.getString("Disclaimer wrap")).append("}");
                css.append(".price-dics-container {").append(footerCss.getString("Price dics container")).append("}");
                css.append("a#fbFooterIcon {").append(footerCss.getString("Facebook icon")).append("}");
                css.append("a#twitterFooterIcon {").append(footerCss.getString("Twitter icon")).append("}");
                css.append("a#gPlusFooterIcon {").append(footerCss.getString("Google plus icon")).append("}");
                css.append("span#PBLogo {").append(footerCss.getString("Footer pb logo")).append("}");

                String delimiter = "\n";
                while (propNames.hasMoreElements()) {
                    String propName = (String) propNames.nextElement();
                    String obj = prop.getProperty(propName);
                    propName = propName.replace("${channelName}", StringUtils.isNotBlank(projectItems.channelName) ? projectItems.channelName: "apparellane");

                    if (propName.endsWith("productName.totalLength"))
                        outputProp.append(propName).append("=").append(productCss.getString("Title length")).append(delimiter);
                    else if (propName.endsWith("nextag.pdir.perpage.list.default"))
                        outputProp.append(propName).append("=").append(p_list.get("Products Count")).append(delimiter);
                    else if (propName.endsWith("nextag.framework.jsp.searchPage.colNo"))
                        outputProp.append(propName).append("=").append(p_list.getString("Per Row Count")).append(delimiter);
                    else if (propName.endsWith("nextag.framework.jsp.outpdir.p_list.display_type"))
                        outputProp.append(propName).append("=").append(p_list.getString("Display type")).append(delimiter);
                    else if (propName.endsWith("nextag.framework.jsp.outpdir.pagination.enabled"))
                        outputProp.append(propName).append("=").append(p_list.get("Pagination")).append(delimiter);
                    else if (propName.endsWith(".framework.opdir.custom.runtime.css"))
                        outputProp.append(propName).append("=").append(css.toString()).append(delimiter);
                    else if (propName.endsWith("nextag.jsp.home.header.categories")) {
                        if(catCss.has("categories")){
                            String format = "nextag.jsp.header.{0}.value={1}";
                            JSONArray categories = catCss.getJSONArray("categories");
                            StringBuilder catList = new StringBuilder();
                            for(int i= 0; i < categories.length(); i++){
                                JSONObject cat = categories.getJSONObject(i);
                                catList.append("cat" + (i+1) + ",");
                                outputProp.append(MessageFormat.format(format, "cat" + (i + 1), cat.get("node"))).append(delimiter);
                            }
                            String catListStr = catList.toString();
                            outputProp.append(propName).append("=").append(catListStr.substring(0,catListStr.lastIndexOf(","))).append(delimiter);
                        }
                    } else
                        outputProp.append(propName).append("=").append(obj).append(delimiter);
                }

                output = outputProp.toString();
            }
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ok(output);
    }

    public static Result loadCategories() {
        JSONArray categories = new JSONArray();
        JSONObject cat1 = new JSONObject();
        cat1.put("name", "Camera");
        cat1.put("node", "500002");
        JSONObject cat2 = new JSONObject();
        cat2.put("name", "Camera&Optics");
        cat2.put("node", "500469");
        JSONObject cat3 = new JSONObject();
        cat3.put("name", "All");
        cat3.put("node", "-1");

        categories.put(cat1);
        categories.put(cat2);
        categories.put(cat3);
        response().setContentType("text/json");
        return ok(categories.toString());
    }
}

package controllers.silhouette.util;

import controllers.adNetwork.util.AppUtil;

/**
 * User: kjoshi
 * Date: 22/12/14
 * Time: 4:35 PM
 */
public interface SilhouetteConstants {
    public static final String title = AppUtil.appConfig().getString("silhouette.title","WizeCommerce Silhouette Builder");
}

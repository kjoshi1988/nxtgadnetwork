package controllers;

import controllers.adNetwork.util.AppConstants;
import org.json.JSONObject;
import play.Routes;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * User: kjoshi
 * Date: 24/12/14
 * Time: 6:07 PM
 */
public class Resources extends Controller{
    public static Result jsRoutes() {
        response().setContentType("text/javascript");
        return ok(Routes.javascriptRouter("jsRoutes", AppConstants.JSRoutes.jsRoutes));
    }

    public static Result getResources(String key, String type, String page){
//        Resource.getInstance().getResourceUrl(key,type);
        return ok();
    }

    public static Result nodes(){
        return ok();
    }
}

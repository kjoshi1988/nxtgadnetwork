package controllers.adNetwork.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import play.Play;

import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class GeneratePdf {
	public static FileInputStream convertTextToPDF(String accessToken, String domain) throws Exception {
		FileInputStream fis = null;
		InputStream in = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		Paragraph para =null;
		try {
			Document pdfDoc = new Document();
			String output_file = System.getProperty("user.home")+"/sample.pdf";
			PdfWriter.getInstance(pdfDoc,new FileOutputStream(output_file));
			pdfDoc.open();
			pdfDoc.setMarginMirroring(true);
			pdfDoc.setMargins(36, 72, 108, 180);
			pdfDoc.topMargin();
			Font myfont = new Font();
			Font bold_font = new Font();
			bold_font.setStyle(Font.BOLD);
			bold_font.setSize(14);
			para = new Paragraph("Wize Commerce Ad Network Technical Document" + "\n", bold_font);
			pdfDoc.add(para);
			myfont.setStyle(Font.NORMAL);
			myfont.setSize(10);
			pdfDoc.add(new Paragraph("\n"));
			in = Play.application().classloader()
					.getResourceAsStream("pdfData/check.txt");
			isr = new InputStreamReader(in);
			br = new BufferedReader(isr);
			String strLine;
			while ((strLine = br.readLine()) != null) {
                strLine = strLine.replace("<:client_id>", accessToken).replace("<:domain>",domain);
				para = new Paragraph(strLine + "\n", myfont);
				System.out.println(strLine);
				pdfDoc.add(para);
			}
            String scriptHtml = "<!DOCTYPE html>\n" +
                    "<html>\n" +
                    "<head>\n" +
                    "    <title></title>\n" +
                    "    <script type=\"text/javascript\">\n" +
                    "        window._wizeAds = {\n" +
                    "            onReady: function () {\n" +
                    "                var block1 = {\n" +
                    "                    clientId: \""+accessToken+"\",\n" +
                    "                    query: \"ipod\",\n" +
                    "                    container: \"container\"\n" +
                    "                };\n"+
                    "                _wizeAds.loadAds(block1);\n" +
                    "            }\n" +
                    "        };\n" +
                    "        (function (N, e, X, T, a, G) {" +
                    "            N[T] = N[T] || {};G = e.getElementsByTagName(X)[0];a = e.createElement(X);a.async = 1;a.src = 'http://"+domain+"/api/scripts/wizeads.js';G.parentNode.insertBefore(a, G);" +
                    "        })(window, document, 'script', '_wizeAds');" +
                    "    </script>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<div id=\"container\"></div>\n" +
                    "</body>\n" +
                    "</html>";
            pdfDoc.add(new Paragraph(scriptHtml, myfont));
			pdfDoc.close();
			fis=new FileInputStream(output_file);
		}
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		} finally {
			if (br != null) {
				br.close();
			}
			if (in != null) {
				in.close();
			}
			if (isr != null) {
				isr.close();
			}
		}
		return fis;
	}
}

package controllers.adNetwork.util;

import play.api.mvc.Call;
import play.core.Router;

/**
 * User: kjoshi
 * Date: 21/11/14
 * Time: 3:39 PM
 */
public interface AppConstants {
    public static final String title = AppUtil.appConfig().getString("adNetwork.title","WizeCommerce AdNetwork");

    public interface URL {
        public static final Call dashBoardCall = controllers.adNetwork.routes.App.index();
        public static final Call loginCall = controllers.adNetwork.authentication.routes.UserAuthController.login();
        public static final Call signInAuthCall = controllers.adNetwork.authentication.routes.UserAuthController.validateUser();
        public static final Call signOutCall = controllers.adNetwork.authentication.routes.UserAuthController.logOut();
        public static final String loginURL = loginCall.url();
        public static final String signInAuthURL = signInAuthCall.url();
        public static final String dashBoardURL = dashBoardCall.url();
        public static final String internalFlag = "internal";
    }

    public interface JSRoutes {
        public static final Router.JavascriptReverseRoute jsRoutes[] = new Router.JavascriptReverseRoute[]{
            controllers.adNetwork.dashboard.routes.javascript.AccessManagementController.showAccessManage(),
            controllers.adNetwork.dashboard.routes.javascript.AccessManagementController.generateAccessToken(),
        	controllers.adNetwork.dashboard.routes.javascript.MerchantDataController.insert(),
        	controllers.adNetwork.dashboard.routes.javascript.MerchantDataController.update(),
        	controllers.adNetwork.dashboard.routes.javascript.MerchantDataController.delete(),
        	controllers.adNetwork.dashboard.routes.javascript.MerchantDataController.getAllAdunits(),
        	controllers.adNetwork.dashboard.routes.javascript.AdsDashBoardController.getProducts(),
        	controllers.routes.javascript.Assets.at(),
        	controllers.adNetwork.wizeAds.routes.javascript.WizeAdsController.getIframeCode(),
        	controllers.adNetwork.wizeAds.routes.javascript.WizeAdsController.generatePdf(),
        	controllers.silhouette.routes.javascript.App.createNewProject(),
        	controllers.silhouette.dashboard.routes.javascript.SilhouetteDashboardController.loadHtmlEditor(),
            controllers.common.api.routes.javascript.BizObj.loadCategories()
        };
    }
}

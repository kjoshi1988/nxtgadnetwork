package controllers.adNetwork.util;

import models.adNetwork.adsWidget.AdDashBoard;
import models.adNetwork.adsWidget.Category;
import models.adNetwork.adsWidget.Discount;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

/**
 * User: kjoshi
 * Date: 05/11/14
 * Time: 5:18 AM
 */
public class StubData {
//    public static AdDashBoard adDashBoard;
    public static void getStubDataForSellerAdWidget() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("columnCount", "5");
        jsonObject.put("rowCount", "1");
        jsonObject.put("defaultBgColor", "#f5f5f5");
        jsonObject.put("bgColor", "#f5f5f5");
        jsonObject.put("categories", getCategories());
        jsonObject.put("discounts", getDiscounts());
        jsonObject.put("keyword", "ipod");
        jsonObject.put("priceDrop", "ipod");

    }

    private static List getCategoriesAsList() {
        List categories = new ArrayList();
        //Arts
        categories.add(getCategoryItem("Arts & Entertainment", 2702147));
        categories.add(getCategoryItem("Baby & Toddler", 2700800));
        categories.add(getCategoryItem("Books", 200000));
        categories.add(getCategoryItem("Business, Industrial & Office", 900000));
        categories.add(getCategoryItem("Clothing & Accessories", 2700200));
        categories.add(getCategoryItem("Computer Software", 300170));
        categories.add(getCategoryItem("Computers", 300000));
        categories.add(getCategoryItem("Electronics", 500000));
        categories.add(getCategoryItem("Flowers & Plants", 2700023));
        categories.add(getCategoryItem("Food & Beverages", 2700035));
        categories.add(getCategoryItem("Furniture", 2700402));
        return categories;

    }
    private static JSONArray getCategories() {
        return new JSONArray(getCategoriesAsList());
    }

    private static JSONArray getDiscounts(){
        return new JSONArray(getDiscountsAsList());
    }

    private static List getDiscountsAsList(){
        List discounts = new ArrayList();
        discounts.add(getDiscountItem("All", "All"));
        discounts.add(getDiscountItem("Free shipping", "_aDeals=Free_Shipping"));
        discounts.add(getDiscountItem("All sales items", "_aDeals=Discount_0"));
        discounts.add(getDiscountItem("10% off and up", "_aDeals=Discount_10"));
        discounts.add(getDiscountItem("Special offers", "_aDeals=Special_offer"));
        discounts.add(getDiscountItem("25% off and up", "_aDeals=Discount_25"));
        discounts.add(getDiscountItem("50% off and up", "_aDeals=Discount_50"));
        return discounts;
    }

    private static Category getCategoryItem(String name, long node){
        Category category = new Category();
        category.name = name;
        category.node = node;
        return category;
    }

    private static Discount getDiscountItem(String label, String value){
        Discount discount = new Discount();
        discount.label = label;
        discount.value = value;
        return discount;
    }

    private static JSONObject getItem(String labelName, String labelValue, String valueLabel, Object value) {
        Map map = new HashMap();
        map.put(labelName, labelValue);
        map.put(valueLabel, value);
        return new JSONObject(map);
    }

    private static Map getItemMap(String labelName, String labelValue, String valueLabel, Object value) {
        Map map = new HashMap();
        map.put(labelName, labelValue);
        map.put(valueLabel, value);
        return map;
    }

    public static AdDashBoard getStubAdsDashBoardData(){
        AdDashBoard adDashBoard = new AdDashBoard();
        adDashBoard.categories = getCategoriesAsList();
        adDashBoard.discounts = getDiscountsAsList();
        adDashBoard.columnCount = 5;
        adDashBoard.rowCount = 1;
        adDashBoard.defaultBgColor = "#f5f5f5";
        adDashBoard.keyword = "ipod";
        adDashBoard.priceDrop ="ipod";
        return adDashBoard;
    }

}

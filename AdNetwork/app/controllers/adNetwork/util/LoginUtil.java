package controllers.adNetwork.util;

import controllers.session.WizeSession;
import play.mvc.Http;

/**
 * User: kjoshi
 * Date: 04/11/14
 * Time: 5:58 AM
 */
public class LoginUtil {
    public static boolean isLoginUrl(Http.Request request) {
        return request.uri().equals(AppConstants.URL.loginURL);
    }

    public static boolean isUserLoggedIn(WizeSession session) {
        return session != null && session.getBoolean("loggedIn");
    }

    public static boolean isSignInAuthReq(Http.Request request, WizeSession session) {
        return request.uri().equals(AppConstants.URL.signInAuthURL) && session != null && !isUserLoggedIn(session);
    }

    public static boolean isFromInternalNetwork(Http.Request request){
       return request.uri().startsWith("/internal");
    }
    public static boolean isForIframe(Http.Request request){
        return request.uri().contains("getIframe");
     }
    public static boolean isForPdf(Http.Request request){
        return request.uri().contains("getPdf");
     }
}

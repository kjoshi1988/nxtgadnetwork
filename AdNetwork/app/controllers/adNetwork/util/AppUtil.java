package controllers.adNetwork.util;

import play.Configuration;
import play.Logger;
import play.Play;

/**
 * User: kjoshi
 * Date: 21/11/14
 * Time: 5:24 PM
 */
public class AppUtil {
    private static final Logger.ALogger logger = Logger.of(AppUtil.class);

    public static Configuration appConfig() {
        return Play.application().configuration();
    }

    public static int getSessionMaxAgeInSeconds() {
        int defaultMaxAge = 30 * 60; //30 minutes
        String sessionTimeOut = appConfig().getString("session.maxAge", "30m");
        try {
            if (sessionTimeOut.endsWith("s"))
                return Integer.parseInt(sessionTimeOut.split("s")[0]);
            if (sessionTimeOut.endsWith("m"))
                return Integer.parseInt(sessionTimeOut.split("m")[0]) * 60;
            if (sessionTimeOut.endsWith("h"))
                return Integer.parseInt(sessionTimeOut.split("h")[0]) * 60 * 60;
            if (sessionTimeOut.endsWith("d"))
                return Integer.parseInt(sessionTimeOut.split("h")[0]) * 60 * 60 * 24;
        } catch (NumberFormatException e) {
            if (logger.isDebugEnabled())
                logger.debug("Invalid session max age value, return default");
        }
        return defaultMaxAge;
    }

    public static long getSessionIdleTimeoutInSeconds() {
        int defaultMaxAge = 30 * 60; //30 minutes
        String sessionTimeOut = appConfig().getString("session.idleTimeout", "30m");
        try {
            if (sessionTimeOut.endsWith("s"))
                return Integer.parseInt(sessionTimeOut.split("s")[0]);
            if (sessionTimeOut.endsWith("m"))
                return Integer.parseInt(sessionTimeOut.split("m")[0]) * 60;
            if (sessionTimeOut.endsWith("h"))
                return Integer.parseInt(sessionTimeOut.split("h")[0]) * 60 * 60;
            if (sessionTimeOut.endsWith("d"))
                return Integer.parseInt(sessionTimeOut.split("h")[0]) * 60 * 60 * 24;
        } catch (NumberFormatException e) {
            if (logger.isDebugEnabled())
                logger.debug("Invalid session timeout value, return default");
        }
        return defaultMaxAge;
    }
}

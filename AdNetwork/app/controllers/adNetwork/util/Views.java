package controllers.adNetwork.util;

/**
 * User: kjoshi
 * Date: 18/11/14
 * Time: 5:16 PM
 */
public enum Views {
    ACCESS_MANAGEMENT,
    ADS_CONTROL_PANEL,
    AUDIENCE,
    VIEW_AD_UNITS,
    CREATE_NEW_AD,
    REAL_TIME;
}

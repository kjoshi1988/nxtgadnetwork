package controllers.adNetwork.dashboard;

import com.adNetwork.service.merchant.MerchantDataServiceImpl;
import com.wizecommerce.adnetwork.merchant.credentials.MerchantCredentials;
import controllers.WizeController;
import controllers.session.WizeSession;
import org.apache.commons.lang.math.NumberUtils;
import play.Logger;
import play.data.Form;
import play.libs.F;
import play.mvc.Result;
import views.html.adNtwrk.dashboard.accessManagement.accessManage;

import java.util.UUID;

/**
 * User: kjoshi
 * Date: 26/11/14
 * Time: 12:42 PM
 */
public class AccessManagementController extends WizeController {
    private final static Logger.ALogger logger = Logger.of(AccessManagementController.class);

    public static F.Promise<Result> showAccessManage() {
        MerchantCredentials merchantCredentials = null;
        WizeSession wizeSession = WizeSession.getCurrentSession(session());
        if (wizeSession != null) {
            merchantCredentials = (MerchantCredentials) wizeSession.get("merchantCredentials");
            if (merchantCredentials == null) {
                Long sellerId = /*(Long) wizeSession.get("sellerId")*/12345678L;
                if (sellerId != null && sellerId != -1) {
                    if (logger.isDebugEnabled()) logger.debug("Fetching credentials from DB for merchant_id=");
                    merchantCredentials = MerchantDataServiceImpl.instance().getMerchantCredentials(sellerId);
                    wizeSession.put("merchantCredentials", merchantCredentials);
                }
            }
        }
        return F.Promise.pure((Result) ok(accessManage.render(merchantCredentials)));
    }

    public static F.Promise<Result> generateAccessToken() {
        long sellerId = NumberUtils.toLong(Form.form().bindFromRequest().get("sellerId"), -1);
        if (logger.isDebugEnabled()) logger.debug("Generating access token for " + sellerId);
        String accessToken = sellerId == -1 ? null : MerchantDataServiceImpl.instance().generateAccessToken(sellerId);
        return F.Promise.pure((Result) ok(accessToken == null ? "Error generating accessToken" : accessToken));
    }
}

package controllers.adNetwork.dashboard;

import com.adNetwork.service.merchant.MerchantDataService;
import com.adNetwork.service.merchant.MerchantDataServiceImpl;
import com.adNetwork.service.thrift.ThriftConstants;
import com.fasterxml.jackson.databind.JsonNode;
import com.wizecommerce.adnetwork.merchant.adunit.AdUnit;
import com.wizecommerce.adnetwork.merchant.adunit.nlatype.NLAType;
import config.ContextConfig;
import controllers.WizeController;
import play.libs.Json;
import play.mvc.Result;
import views.html.adNtwrk.merchantdata;

import java.util.ArrayList;
import java.util.List;

import org.apache.thrift.TSerializer;
import org.apache.thrift.protocol.TSimpleJSONProtocol;
import org.json.JSONArray;
import org.json.JSONObject;

public class MerchantDataController extends WizeController {
    public static Result index() {
        MerchantDataService service = MerchantDataServiceImpl.instance();
        System.out.println("+++" + service.getAdUnit(1, "", "1").getDisplayText());
        ContextConfig.put("merchantData", service.getAdUnit(1, "", "1"));
        return ok(merchantdata.render("Merchant Data"));
    }

    public static Result create() {
        ContextConfig.put("merchantData", new AdUnit());
        return ok(merchantdata.render("Create Merchant Data"));
    }

    public static Result getAllAdunits(Long sellerid, String domain) {
        System.out.println("+++" + sellerid);
        System.out.println("+++" + domain);
        MerchantDataService service = MerchantDataServiceImpl.instance();
        System.out.println(ThriftConstants.THRIFT_HOST);
        TSerializer serializer = new TSerializer(new TSimpleJSONProtocol.Factory());
        List<AdUnit> adunits=service.getAllAdUnits(sellerid, domain);
        JSONArray adUnitJson=new JSONArray();
        for(AdUnit adunit:adunits){
        	JSONObject obj=new JSONObject();
        	obj.put("adUnitId", adunit.getAdUnitId());
        	obj.put("adResponseType", adunit.getAdResponseType());
        	obj.put("resultCount", adunit.getResultCount());
        	obj.put("modeType", adunit.getModeType());
        	obj.put("columns", adunit.getColumns());
        	obj.put("rows", adunit.getRows());
        	obj.put("backgroundColor", adunit.getBackgroundColor());
        	obj.put("node", adunit.getNode());
        	obj.put("keyword", adunit.getKeyword());
        	obj.put("displayText", adunit.getDisplayText());
        	obj.put("layoutType", adunit.getLayoutType());
        	JSONArray nlaJson=new JSONArray();
        	for(NLAType nla:adunit.getNlaTypes()){
        		JSONObject nlaobj=new JSONObject();
        		nlaobj.put("nlaName", nla.getNlaName());
        		nlaobj.put("nlaValue", nla.getNlaValue());
        		nlaJson.put(nlaobj);
        	}
        	obj.put("nlaTypes", nlaJson);
        	adUnitJson.put(obj);
        }
        return ok(adUnitJson.toString()).as("application/json");
    }

    public static Result insert() {
        JsonNode jsonObj = request().body().asJson();
        AdUnit obj = Json.fromJson(jsonObj, AdUnit.class);
        System.out.println(obj.getDisplayText());
        MerchantDataService service = MerchantDataServiceImpl.instance();
        obj.setResultCount(obj.getRows()*obj.getColumns());
        AdUnit adUnit = service.insertAdUnit(12345678, "www.nextag.com", obj);
//        System.out.println(obj.getNlaTypes());
//        System.out.println(service.insertAdUnit(12345678, "www.nextag.com", obj));
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("adUnitId", adUnit.getAdUnitId());
        return ok(jsonObject.toString()).as("application/json");
    }
    public static Result update() {
        JsonNode jsonObj = request().body().asJson();
        AdUnit obj = Json.fromJson(jsonObj, AdUnit.class);
        System.out.println(obj.getDisplayText());
        MerchantDataService service = MerchantDataServiceImpl.instance();
        obj.setResultCount(obj.getRows()*obj.getColumns());
        System.out.println("update");
        System.out.println(obj.getNlaTypes());
        System.out.println(service.updateAdUnit(12345678, "www.nextag.com", obj));
        return ok();
    }
    public static Result delete(String adUnitId){
    	MerchantDataService service = MerchantDataServiceImpl.instance();
    	System.out.println("===="+adUnitId);
    	System.out.println(service.deleteAdUnit(12345678, "www.nextag.com", adUnitId));
    	return ok();
    }
}

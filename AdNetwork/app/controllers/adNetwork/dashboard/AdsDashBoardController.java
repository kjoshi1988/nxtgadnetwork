package controllers.adNetwork.dashboard;

import com.adNetwork.service.adData.AdDataService;
import com.adNetwork.service.adData.AdDataServiceImpl;
import controllers.WizeController;
import play.libs.Json;
import play.libs.Yaml;
import play.mvc.Result;

/**
 * User: kjoshi
 * Date: 10/11/14
 * Time: 2:33 PM
 */
public class AdsDashBoardController extends WizeController {
    public static Result defaults() {
        return ok(Json.toJson(Yaml.load("testData/adDashBoard.yml")));
    }

    public static Result getProducts(String queryString,int row,int column) {
        return ok(AdDataServiceImpl.instance().getProductsAsJson(queryString,row,column).toString()).as("application/json");
    }
}

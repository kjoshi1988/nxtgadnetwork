package controllers.adNetwork.authentication;

import com.adNetwork.service.merchant.MerchantDataServiceImpl;
import com.adNetwork.service.user.UserAuthService;
import com.adNetwork.service.user.UserServiceAuthImpl;
import com.wizecommerce.adnetwork.merchant.credentials.MerchantCredentials;
import controllers.WizeController;
import controllers.adNetwork.action.UserAuth;
import controllers.session.WizeSession;
import controllers.adNetwork.util.AppConstants;
import play.Logger;
import play.data.Form;
import play.libs.F;
import play.mvc.Result;
import views.html.adNtwrk.authenticate.login;

/**
 * User: kjoshi
 * Date: 04/11/14
 * Time: 4:20 AM
 */
public class UserAuthController extends WizeController {
    private final static Logger.ALogger logger = Logger.of(UserAuthController.class);
    private final static UserAuthService userAuthService = new UserServiceAuthImpl();

    @UserAuth.RedirectIfAuthenticated
    public static Result login(){
        return ok(login.render(AppConstants.title));
    }
    public static F.Promise<Result> validateUser(){
        if(userAuthService.validateUser(Form.form().bindFromRequest().get("uname"), Form.form().bindFromRequest().get("pwd"))){
            WizeSession wizeSession = WizeSession.getCurrentSession(session());
            if(wizeSession != null){
                long sellerId = 32132;
                wizeSession.put("uname", Form.form().bindFromRequest().get("uname"));
                wizeSession.put("sellerId", sellerId);
                if (logger.isDebugEnabled()) logger.debug("Fetching credentials from DB for merchant_id=" + sellerId);
                MerchantCredentials merchantCredentials = MerchantDataServiceImpl.instance().getMerchantCredentials(sellerId);
                System.out.println("got response from service");
                wizeSession.put("merchantCredentials", merchantCredentials);
                wizeSession.put("loggedIn", true);
            }
            return F.Promise.pure((Result)ok("true"));
        }
        return F.Promise.pure((Result)ok("false"));
    }

    @UserAuth.LogoutAuthentication
    public static F.Promise<Result> logOut(){
        WizeSession wizeSession = WizeSession.getCurrentSession(session());
        if(wizeSession != null && wizeSession.getBoolean("loggedIn")){
            wizeSession.invalidate();
        }
        return F.Promise.pure(redirect(AppConstants.URL.loginCall));
    }
}

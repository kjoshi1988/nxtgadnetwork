package controllers.adNetwork.wizeAds;

import com.adNetwork.service.adData.AdDataService;
import com.adNetwork.service.adData.AdDataServiceImpl;
import com.adNetwork.service.merchant.MerchantDataService;
import com.adNetwork.service.merchant.MerchantDataServiceImpl;
import com.wizecommerce.adnetwork.merchant.adunit.AdUnit;
import com.wizecommerce.adnetwork.merchant.adunit.nlatype.NLAType;
import com.wizecommerce.adnetwork.merchant.credentials.MerchantCredentials;
import config.util.UrlWrapper;
import controllers.adNetwork.util.GeneratePdf;
import models.adNetwork.adsWidget.AdStyle;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import play.data.Form;
import play.libs.F;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.adNtwrk.adContentHtml;

/**
 * User: kjoshi
 * Date: 02/12/14
 * Time: 3:59 PM
 */
public class WizeAdsController extends Controller {
    public static F.Promise<Result> getIframeCode(final String adUnitId) {
        return F.Promise.promise(new F.Function0<Result>() {
            @Override
            public Result apply() throws Throwable {
                System.out.println("code comes" + adUnitId);
                MerchantDataService service = MerchantDataServiceImpl.instance();
                AdUnit adunit = service.getAdUnit(12345678, "www.nextag.com", adUnitId);
                String queryString = "search=" + adunit.getKeyword() + "&node=" + adunit.getNode();
                if (adunit.getNlaTypes() != null && adunit.getNlaTypes().size() > 0) {
                    for (NLAType nla : adunit.getNlaTypes()) {
                        queryString += nla.getNlaValue();
                    }
                }
                System.out.println("quwerystring====" + queryString);
                return ok(adContentHtml.render(adunit, AdDataServiceImpl.instance().getProductsAsMap(queryString, adunit.getRows(), adunit.getColumns(), true)));
            }
        });
    }

    public static F.Promise<Result> generatePdf(final String adId) throws Exception {
        return F.Promise.promise(new F.Function0<Result>() {
            @Override
            public Result apply() throws Throwable {
                MerchantCredentials merchantCredentials = MerchantDataServiceImpl.instance().getMerchantCredentials(12345678L);
                return ok(GeneratePdf.convertTextToPDF(merchantCredentials.getAccessToken() + "@" + adId, request().getHeader("host"))).as("application/pdf");
            }
        });
    }

    public static F.Promise<Result> productsViaScript(String query, String accessToken) {
        AdStyle adStyle = decode(Form.form().bindFromRequest().get("styles"));
        String adUnitId = accessToken.split("@")[1];
        return getIframeCode(adUnitId);
    }

    private static AdStyle decode(String styles) {
        AdStyle adStyle = new AdStyle();
        if (StringUtils.isNotBlank(styles)) {
            for (String s : styles.split("@@@")) {
                String properties[] = s.split("@@");
                if (properties.length == 2) {
                    String propertyName = decodeProperty(properties[0]);
                    String propertyValue = decodeProperty(properties[1]);
                    switch (Enum.valueOf(AdStyle.AdStyleEnum.class, propertyName)) {
                        case cardStyle:
                            adStyle.setCardStyle(decodeProperty(propertyValue));
                            break;
                        case fontFamily:
                            adStyle.setFontFamily(decodeProperty(propertyValue));
                            break;
                        case headerStyle:
                            adStyle.setHeaderStyle(decodeProperty(propertyValue));
                            break;
                        case priceStyle:
                            adStyle.setPriceStyle(decodeProperty(propertyValue));
                            break;
                        case titleStyle:
                            adStyle.setTitleStyle(decodeProperty(propertyValue));
                            break;
                        case sellerStyle:
                            adStyle.setSellerStyle(decodeProperty(propertyValue));
                    }
                }
            }
        }
        return adStyle;
    }

    private static String decodeProperty(String encodeStr) {
        StringBuilder decodedStr = new StringBuilder();
        for (String str : encodeStr.split("@")) {
            int code = NumberUtils.toInt(str, -1);
            if (code != -1 && ((code >= 97 && code <= 122) || (code >= 65 && code <= 90)))
                decodedStr.append(Character.toChars(code));
            else decodedStr.append(str);
        }
        return decodedStr.toString();
    }

    public static Result recordClick(String clickTrackUrl) {
        return redirect(UrlWrapper.decode(clickTrackUrl));
    }


}

package controllers.adNetwork.wizeAds;

import javax.xml.bind.DatatypeConverter;
import java.io.*;

/**
 * User: kjoshi
 * Date: 02/12/14
 * Time: 4:03 PM
 */
public class Token implements Serializable{
    private long sellerId;
    private String domain;
    private String accessToken;

    public Token() {
    }

    public Token(long sellerId, String domain, String accessToken) {
        this.sellerId = sellerId;
        this.domain = domain;
        this.accessToken = accessToken;
    }

    public long getSellerId() {
        return sellerId;
    }

    public void setSellerId(long sellerId) {
        this.sellerId = sellerId;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public static String writeToString(Token token) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(byteArrayOutputStream);
            oos.writeObject(token);
            oos.close();
            return DatatypeConverter.printBase64Binary(byteArrayOutputStream.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Token readFromString(String tokenString) {
        try {
            byte[] data = DatatypeConverter.parseBase64Binary(tokenString);
            ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(data));
            Object obj = objectInputStream.readObject();
            objectInputStream.close();
            if (obj instanceof Token)
                return (Token) obj;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}

package controllers.adNetwork;

import config.ContextConfig;
import controllers.WizeController;
import controllers.adNetwork.action.UserAuth;
import controllers.adNetwork.util.AppConstants;
import play.Routes;
import play.libs.Yaml;
import play.mvc.Result;
import views.html.adNtwrk.app;

/**
 * User: kjoshi
 * Date: 04/11/14
 * Time: 4:26 AM
 */
public class App extends WizeController {

    //@UserAuth.Authenticate
    public static Result index() {
        init();
        return ok(app.render(AppConstants.title));
    }

    private static void init() {
        ContextConfig.put("adDashBoard", Yaml.load("testData/adDashBoard.yml"));
        ContextConfig.put("sideNavItems", Yaml.load("testData/sideNavItems.yml"));
        //AdDataService adDataService = AdDataUtil.getStubAdService();
        //ContextConfig.put("adUnits",adDataService.getAllAdUnits(1,"Test"));
    }
}

package controllers.adNetwork.action;

import controllers.session.WizeSession;
import controllers.adNetwork.util.AppConstants;
import controllers.adNetwork.util.LoginUtil;
import play.Logger;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * User: kjoshi
 * Date: 21/11/14
 * Time: 3:03 PM
 */
public class UserAuth {
    private static final Logger.ALogger logger = Logger.of(UserAuth.class);

    public static class AuthenticateUser extends Action<Authenticate> {
        @Override
        public F.Promise<Result> call(Http.Context ctx) throws Throwable {
            if(!LoginUtil.isUserLoggedIn(WizeSession.getCurrentSession(ctx.session()))){
                if (logger.isDebugEnabled()) logger.debug("User not logged in, redirecting to login page");
                return F.Promise.pure(redirect(AppConstants.URL.loginCall));
            }
            return delegate.call(ctx);
        }
    }

    public static class RedirectToHomeIfAuthenticated extends Action<RedirectIfAuthenticated> {
        @Override
        public F.Promise<Result> call(Http.Context ctx) throws Throwable {
            if(LoginUtil.isUserLoggedIn(WizeSession.getCurrentSession(ctx.session()))){
                if (logger.isDebugEnabled()) logger.debug("User already logged in, redirecting to home page");
                return F.Promise.pure(redirect(AppConstants.URL.dashBoardCall));
            }
            return delegate.call(ctx);
        }
    }

    public static class AuthenticateLogout extends Action<LogoutAuthentication> {
        @Override
        public F.Promise<Result> call(Http.Context ctx) throws Throwable {
            if(!LoginUtil.isUserLoggedIn(WizeSession.getCurrentSession(ctx.session()))){
                if (logger.isDebugEnabled()) logger.debug("User not logged in, Invalid request");
                return F.Promise.pure((Result)badRequest());
            }
            return delegate.call(ctx);
        }
    }

    @With(AuthenticateUser.class)
    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public static @interface Authenticate {
        AnnotationType value() default AnnotationType.AUTH;
    }

    @With(RedirectToHomeIfAuthenticated.class)
    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public static @interface RedirectIfAuthenticated {
        AnnotationType value() default AnnotationType.RedirectIfLoginAuthenticated;
    }

    @With(AuthenticateLogout.class)
    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public static @interface LogoutAuthentication {
    }

    private enum AnnotationType{
        AUTH,
        RedirectIfLoginAuthenticated
    }
}

package controllers.adNetwork.action;

import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * User: kjoshi
 * Date: 02/12/14
 * Time: 3:39 PM
 */
public class TokenAuthAction extends Action<TokenAuthAction>{
    @Override
    public F.Promise<Result> call(Http.Context context) throws Throwable {

        return delegate.call(context);
    }

    @With(TokenAuthAction.class)
    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public static @interface Authenticate{

    }
}

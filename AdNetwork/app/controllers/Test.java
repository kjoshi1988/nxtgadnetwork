package controllers;

import models.silhouette.ProjectItemData;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

/**
 * User: kjoshi
 * Date: 30/12/14
 * Time: 5:33 PM
 */
public class Test {
    public static void main(String[] args) throws IOException {
        Properties prop = new Properties();
        Properties outputProp = new Properties();

        String filePath = System.getProperty("user.dir") + "/conf/testData/silhouette/template.tmp";
        FileInputStream fis = new FileInputStream(new File(filePath));
        ProjectItemData projectItemData = ProjectItemData.loadDefault("1");

        prop.load(fis);
        Enumeration propNames = prop.propertyNames();
        JSONObject p_list = new JSONObject(projectItemData.p_listCss);
        JSONObject productCss = new JSONObject(projectItemData.productCss);
        JSONObject bodyCss = new JSONObject(projectItemData.bodyCss);
        JSONObject headerCss = new JSONObject(projectItemData.headerCss);
        JSONObject logoCss = new JSONObject(projectItemData.logoCss);
        JSONObject searchCss = new JSONObject(projectItemData.searchCss);
        JSONObject catCss = new JSONObject(projectItemData.categoriesCss);
        JSONObject nlaCss = new JSONObject(projectItemData.nlaCss);
        JSONObject footerCss = new JSONObject(projectItemData.footerCss);


        StringBuilder css = new StringBuilder();

        //bodyCss
        css.append("body{").append(bodyCss.getString("Background CSS")).append("}");
        //headerCss
        css.append(".mainDiv{").append(headerCss.getString("Background CSS")).append("}");
        //logoCss
        css.append(".logoWrap a.logoWrap_a{").append(logoCss.getString("CSS")).append("}");
        //searchCss
        css.append(".searchBoxWrap div.searchBox{").append(searchCss.getString("Search box")).append("}");
        css.append(".searchFrm div.textWrap{").append(searchCss.getString("Box wrap css")).append("}");
        css.append("#searchInput{").append(searchCss.getString("Box css")).append("}");
        css.append(".searchFrm div.submtImgWrap{").append(searchCss.getString("Button wrap css")).append("}");
        css.append(".submtImgWrap .submtImg{").append(searchCss.getString("Button css")).append("}");

        //categoryCss
        css.append(".mainDiv div.headerContainer {").append(catCss.getString("Wrap css")).append("}");
        css.append(".menuBar div.menuRootNode {").append(catCss.getString("Node block css")).append("}");
        css.append(".menuRootNode a.shineAnchor {").append(catCss.getString("Node css")).append("}");
        css.append("ul.subMenu_ul {").append(catCss.getString("Submenu container")).append("}");
        css.append(".subMenu_ul li.subMenu_li {").append(catCss.getString("Submenu item")).append("}");
        css.append(".subMenu_ul li.subMenu_li:hover  {").append(catCss.getString("Submenu item hover")).append("}");
        css.append(".subMenu_li a.subMenu_a {").append(catCss.getString("Submenu link")).append("}");
        css.append(".subMenu_li a.subMenu_a:hover {").append(catCss.getString("Submenu link hover")).append("}");

        //nlaCss
        css.append(".search_results_content_class div.nlaWrap{").append(nlaCss.getString("Wrap css")).append("}");
        css.append(".nlaWrap .floatLeft#filterText {").append(nlaCss.getString("Refine text css")).append("}");
        css.append(".matchesString#resultsMessage{").append(nlaCss.getString("Pagination info css")).append("}");
        css.append(".nlaContainer ul.nlaList{").append(nlaCss.getString("Sub nla container")).append("}");
        css.append(".nlaContainer .nlaList li.nlaRow-level-0{").append(nlaCss.getString("Sub nla item")).append("}");
        css.append(".nlaContainer .nlaList li.nlaRow-level-0:hover{").append(nlaCss.getString("Sub nla item hover")).append("}");

        //product css
        css.append(".li_float.row_basic_style.row_li {").append(p_list.getString("Product Container Css")).append("}");

        //product block css
        css.append(".imgContainer {").append(productCss.getString("Image css")).append("}");
        css.append(".titleName {").append(productCss.getString("Title css")).append("}");
        css.append("div.price-range-grid{").append(productCss.getString("Price css")).append("}");
        css.append(".seller-info-grid b {").append(productCss.getString("Seller name css")).append("}");
        css.append(".wizeRating.blankStar {").append(productCss.getString("Rating blank star css")).append("}");
        css.append(".wizeRating.fullStar{").append(productCss.getString("Rating full star css")).append("}");
        css.append(".wizeRating.halfStar{").append(productCss.getString("Rating half star css")).append("}");

        //footer css
        css.append(".footerLinkWrap {").append(footerCss.getString("Nav css")).append("}");
        css.append(".disclaimer {").append(footerCss.getString("Disclaimer wrap")).append("}");
        css.append(".price-dics-container {").append(footerCss.getString("Price dics container")).append("}");
        css.append("a#fbFooterIcon {").append(footerCss.getString("Facebook icon")).append("}");
        css.append("a#twitterFooterIcon {").append(footerCss.getString("Twitter icon")).append("}");
        css.append("a#gPlusFooterIcon {").append(footerCss.getString("Google plus icon")).append("}");
        css.append("span#PBLogo {").append(footerCss.getString("Footer pb logo")).append("}");


        while (propNames.hasMoreElements()) {
            String propName = (String) propNames.nextElement();
            propName = propName.replace("${channelName}", "psmus");

            if (propName.endsWith("productName.totalLength"))
                outputProp.setProperty(propName, productCss.getString("Title length"));
            else if (propName.endsWith("nextag.pdir.perpage.list.default"))
                outputProp.setProperty(propName, p_list.getString("Products Count"));
            else if (propName.endsWith("nextag.framework.jsp.searchPage.colNo"))
                outputProp.setProperty(propName, p_list.getString("Per Row Count"));
            else if (propName.endsWith("nextag.framework.jsp.outpdir.p_list.display_type"))
                outputProp.setProperty(propName, p_list.getString("Display type"));
            else if (propName.endsWith("nextag.framework.jsp.outpdir.pagination.enabled"))
                outputProp.setProperty(propName, p_list.getString("Pagination"));
            else if (propName.endsWith(".framework.opdir.custom.runtime.css"))
                outputProp.setProperty(propName, css.toString());
            outputProp.setProperty(propName, prop.getProperty(propName));
        }

        outputProp.toString();
        fis.close();
    }
}

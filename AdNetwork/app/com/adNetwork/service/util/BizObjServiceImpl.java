package com.adNetwork.service.util;

import com.adNetwork.service.thrift.ThriftClient;
import com.adNetwork.service.thrift.ThriftServiceUtil;
import com.wizecommerce.adnetwork.common.INode;
import com.wizecommerce.service.adnetwork.AdNetworkService;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;

import java.util.Map;

/**
 * User: kjoshi
 * Date: 02/01/15
 * Time: 5:22 PM
 */
public class BizObjServiceImpl implements BizObjService {
    private static BizObjServiceImpl _this;
    private BizObjServiceImpl() {}
    public static BizObjServiceImpl instance() {
        if (_this == null)
            _this = new BizObjServiceImpl();
        return _this;
    }

    @Override
    public Map<Long, INode> fetchCategories(final short depth) {
        return ThriftServiceUtil.call(new ThriftClient<Map<Long, INode>>() {
            @Override
            public Map<Long, INode> getClientData(TBinaryProtocol tBinaryProtocol) throws TException {
                AdNetworkService.Client client = new AdNetworkService.Client(tBinaryProtocol);
                return client.fetchCategories(depth);
            }
        });
    }
}

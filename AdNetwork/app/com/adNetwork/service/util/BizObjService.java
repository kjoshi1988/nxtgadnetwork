package com.adNetwork.service.util;

import com.wizecommerce.adnetwork.common.INode;

import java.util.Map;

/**
 * User: kjoshi
 * Date: 02/01/15
 * Time: 5:22 PM
 */
public interface BizObjService {
    Map<Long, INode> fetchCategories(short depth);
}

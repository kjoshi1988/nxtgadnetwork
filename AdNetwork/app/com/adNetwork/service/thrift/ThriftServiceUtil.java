package com.adNetwork.service.thrift;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import play.Logger;

import java.util.Date;

/**
 * User: kjoshi
 * Date: 19/11/14
 * Time: 12:02 PM
 */
public class ThriftServiceUtil {
    private static final Logger.ALogger logger = Logger.of(ThriftServiceUtil.class);

    public static <Generic> Generic call(ThriftClient<Generic> thriftClient) {
        long start_time = new Date().getTime();
        Generic genericObj = null;
        try {
            TTransport transport = new TSocket(ThriftConstants.THRIFT_HOST, ThriftConstants.THRIFT_PORT);
            transport.open();
            genericObj = thriftClient.getClientData(new TBinaryProtocol(transport));
            transport.close();
        } catch (TTransportException e) {
            e.printStackTrace();
        } catch (TException e) {
            e.printStackTrace();
        }
        if (logger.isDebugEnabled()) {
            StackTraceElement stackTraceElements[] = Thread.currentThread().getStackTrace();
            logger.debug("Thrift Query Time for (" + (stackTraceElements.length > 2 ? stackTraceElements[2] : stackTraceElements[0]) + "):" + (new Date().getTime() - start_time) + "ms");
        }
        return genericObj;
    }
}

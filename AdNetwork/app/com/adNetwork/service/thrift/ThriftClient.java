package com.adNetwork.service.thrift;

import com.wizecommerce.service.common.InvalidRequestException;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;

/**
 * User: kjoshi
 * Date: 19/11/14
 * Time: 2:13 PM
 */
public interface ThriftClient<Generic> {
    Generic getClientData(TBinaryProtocol tBinaryProtocol) throws TException;
}

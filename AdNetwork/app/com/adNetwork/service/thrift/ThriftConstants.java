package com.adNetwork.service.thrift;

import controllers.adNetwork.util.AppUtil;

/**
 * User: kjoshi
 * Date: 19/11/14
 * Time: 12:22 PM
 */
public interface ThriftConstants {
    public final static String THRIFT_HOST = AppUtil.appConfig().getString("adNetwork.thrift.host","localhost");
    public final static int THRIFT_PORT = AppUtil.appConfig().getInt("adNetwork.thrift.port",9090);
}

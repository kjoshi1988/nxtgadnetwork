package com.adNetwork.service.merchant;

import com.wizecommerce.adnetwork.merchant.adunit.AdUnit;
import com.wizecommerce.adnetwork.merchant.credentials.MerchantCredentials;

import java.util.List;

/**
 * User: kjoshi
 * Date: 19/11/14
 * Time: 2:39 PM
 */
public interface MerchantDataService {
    AdUnit getAdUnit(long sellerId, String domain, String adUnitId);
    List<AdUnit> getAllAdUnits(long sellerId, String domain);
    public AdUnit insertAdUnit(final long sellerId, final String domain, final AdUnit adUnit);
    public boolean updateAdUnit(final long sellerId, final String domain, final AdUnit adUnit);
    public String generateAccessToken(long sellerId);
    public MerchantCredentials getMerchantCredentials(long sellerId);
    public boolean deleteAdUnit(long sellerId, String domain, String adUnitId);
}

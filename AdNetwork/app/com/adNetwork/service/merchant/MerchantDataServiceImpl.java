package com.adNetwork.service.merchant;

import com.adNetwork.service.thrift.ThriftClient;
import com.adNetwork.service.thrift.ThriftServiceUtil;
import com.wizecommerce.adnetwork.merchant.adunit.AdUnit;
import com.wizecommerce.adnetwork.merchant.credentials.MerchantCredentials;
import com.wizecommerce.service.adnetwork.AdNetworkService;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;

import java.util.List;

public class MerchantDataServiceImpl implements MerchantDataService {
    private static MerchantDataServiceImpl _this;
    private MerchantDataServiceImpl(){}
    public static MerchantDataServiceImpl instance() {
        if (_this == null)
            _this = new MerchantDataServiceImpl();
        return _this;
    }

    @Override
    public AdUnit getAdUnit(final long sellerId, final String domain, final String adUnitId) {
        return ThriftServiceUtil.call(new ThriftClient<AdUnit>() {
            @Override
            public AdUnit getClientData(TBinaryProtocol tBinaryProtocol) throws TException {
                AdNetworkService.Client client = new AdNetworkService.Client(tBinaryProtocol);
                return client.getAdUnitForSellerDomain(sellerId, domain, adUnitId);
            }
        });
    }

    @Override
    public List<AdUnit> getAllAdUnits(final long sellerId, final String domain) {
        return ThriftServiceUtil.call(new ThriftClient<List<AdUnit>>() {
            @Override
            public List<AdUnit> getClientData(TBinaryProtocol tBinaryProtocol) throws TException {
                AdNetworkService.Client client = new AdNetworkService.Client(tBinaryProtocol);
                return client.getAllAdUnitsForDomain(sellerId, domain);
            }
        });
    }

    @Override
    public AdUnit insertAdUnit(final long sellerId, final String domain, final AdUnit adUnit) {
        return ThriftServiceUtil.call(new ThriftClient<AdUnit>() {
            @Override
            public AdUnit getClientData(TBinaryProtocol tBinaryProtocol) throws TException {
                AdNetworkService.Client client = new AdNetworkService.Client(tBinaryProtocol);
                return client.createAdUnitForSellerDomain(sellerId, domain, adUnit);
            }
        });
    }

    @Override
    public String generateAccessToken(final long sellerId) {
        return ThriftServiceUtil.call(new ThriftClient<String>() {
            @Override
            public String getClientData(TBinaryProtocol tBinaryProtocol) throws TException {
                AdNetworkService.Client client = new AdNetworkService.Client(tBinaryProtocol);
                return client.generateNewAccessToken(sellerId);
            }
        });
    }

    @Override
    public MerchantCredentials getMerchantCredentials(final long sellerId) {
        return ThriftServiceUtil.call(new ThriftClient<MerchantCredentials>() {
            @Override
            public MerchantCredentials getClientData(TBinaryProtocol tBinaryProtocol) throws TException {
                AdNetworkService.Client client = new AdNetworkService.Client(tBinaryProtocol);
                return client.getMerchantCredentials(sellerId);
            }
        });
    }
	@Override
	public boolean updateAdUnit(final long sellerId, final String domain, final AdUnit adUnit) {
        return ThriftServiceUtil.call(new ThriftClient<Boolean>() {
            @Override
            public Boolean getClientData(TBinaryProtocol tBinaryProtocol) throws TException {
                AdNetworkService.Client client = new AdNetworkService.Client(tBinaryProtocol);
                return client.updateAdUnitForSellerDomain(sellerId, domain, adUnit);
            }
        });
    }
	@Override
	public boolean deleteAdUnit(final long sellerId, final String domain, final String adUnit) {
        return ThriftServiceUtil.call(new ThriftClient<Boolean>() {
            @Override
            public Boolean getClientData(TBinaryProtocol tBinaryProtocol) throws TException {
                AdNetworkService.Client client = new AdNetworkService.Client(tBinaryProtocol);
                return client.deleteAdUnitForSellerDomain(sellerId, domain, adUnit);
            }
        });
    }
}

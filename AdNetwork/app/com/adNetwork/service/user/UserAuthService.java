package com.adNetwork.service.user;

import org.json.JSONObject;

/**
 * User: kjoshi
 * Date: 19/11/14
 * Time: 12:01 PM
 */
public interface UserAuthService {
    boolean validateUser(String username, String password);
}

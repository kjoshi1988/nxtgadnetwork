package com.adNetwork.service.adData;


import com.wizecommerce.adnetwork.common.Product;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * User: kjoshi
 * Date: 19/11/14
 * Time: 2:39 PM
 */
public interface AdDataService {
    List<Product> getProducts(String queryString);
    List<Product> getProducts(String queryString, boolean fetchClickTrackUrl);
    JSONObject getProductsAsJson(String queryString,int row,int column);
    JSONObject getProductsAsJson(String queryString,int row,int column, boolean fetchClickTrackUrl);
    Map<Short, List<Product>> getProductsAsMap(String queryString,int row,int column);
    Map<Short, List<Product>> getProductsAsMap(String queryString,int row,int column, boolean fetchClickTrackUrl);
}

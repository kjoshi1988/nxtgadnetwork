package com.adNetwork.service.adData;

import com.adNetwork.service.thrift.ThriftClient;
import com.adNetwork.service.thrift.ThriftServiceUtil;
import com.wizecommerce.adnetwork.common.Product;
import com.wizecommerce.service.adnetwork.AdNetworkService;
import config.util.JSONUtil;
import config.util.UrlWrapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdDataServiceImpl implements AdDataService {
    private static AdDataServiceImpl _this;
    private static final String SINGLE_TAG_PARAM_NAME = "fetchSingleTag";
    private static final String CLICK_TRACK_URL_PARAM_NAME = "fetchClickTrackUrl";

    private AdDataServiceImpl() {
    }

    public static AdDataServiceImpl instance() {
        if (_this == null)
            _this = new AdDataServiceImpl();
        return _this;
    }

    @Override
    public List<Product> getProducts(String queryString) {
        return getProducts(queryString, false);
    }

    @Override
    public List<Product> getProducts(final String queryString, final boolean fetchClickTrackUrl) {
        return ThriftServiceUtil.call(new ThriftClient<List<Product>>() {
            @Override
            public List<Product> getClientData(TBinaryProtocol tBinaryProtocol) throws TException {
                return new AdNetworkService.Client(tBinaryProtocol).getProducts(getQueryString(queryString, fetchClickTrackUrl));
            }
        });
    }

    @Override
    public Map<Short, List<Product>> getProductsAsMap(String queryString, int row, int column) {
        return getProductsAsMap(queryString, row, column, false);
    }

    public Map<Short, List<Product>> getProductsAsMap(String queryString, int row, int column, boolean fetchClickTrackUrl) {
        List<Product> products = getProducts(queryString, fetchClickTrackUrl);
        Map<Short, List<Product>> productsMap = new HashMap<Short, List<Product>>();
        if (products != null && !products.isEmpty()) {
            int productCount = products.size(), index = 0;
            for (short rowCount = 0; rowCount < row && index < productCount; rowCount++) {
                List<Product> prodList = new ArrayList<Product>();
                for (int colCount = 0; colCount < column && index < productCount; colCount++) {
                    Product product = products.get(index++);
                    if(StringUtils.isNotBlank(product.getClickURL()))
                        product.setClickURL(UrlWrapper.encode(product.getClickURL()));
                    prodList.add(product);
                }
                productsMap.put(rowCount, prodList);
            }
        }
        return productsMap;
    }

    @Override
    public JSONObject getProductsAsJson(String queryString, int row, int column) {
        return getProductsAsJson(queryString, row, column, false);
    }

    @Override
    public JSONObject getProductsAsJson(String queryString, int row, int column, boolean fetchClickTrackUrl) {
        List<Product> products = getProducts(queryString, fetchClickTrackUrl);
        JSONObject productsMap = new JSONObject();
        if(products != null && !products.isEmpty()){
            int productCount = products.size(), index = 0;
            for(short rowCount = 0; rowCount < row && index < productCount; rowCount++){
                JSONArray prodList = new JSONArray();
                for(int colCount = 0; colCount < column && index < productCount; colCount++){
                    prodList.put(JSONUtil.productToJson(products.get(index++)));
                }
                productsMap.put(String.valueOf(rowCount), prodList);
            }
        }
        return productsMap;
    }

    private String getQueryString(String queryString, boolean addClickTrackParam){
        boolean hasSingleTagOnly = queryString.contains(SINGLE_TAG_PARAM_NAME);
        addClickTrackParam = addClickTrackParam && !queryString.contains(SINGLE_TAG_PARAM_NAME);
        boolean endsWithAmp = queryString.endsWith("&");
        StringBuilder paramToBeAdded = new StringBuilder();
        if(!hasSingleTagOnly)
            paramToBeAdded.append(SINGLE_TAG_PARAM_NAME).append("=").append("true").append("&");
        if(addClickTrackParam)
            paramToBeAdded.append(CLICK_TRACK_URL_PARAM_NAME).append("=").append("true");
        return queryString + (endsWithAmp ? "" : "&") + paramToBeAdded.toString();
    }
}

package models.adNetwork.sideNav;

import controllers.adNetwork.util.Views;

import java.util.List;

/**
 * User: kjoshi
 * Date: 14/11/14
 * Time: 4:06 PM
 */
public class SideNavItem {
    public String label;
    public int sno;
    public Views viewAction;
    public String click;
//    public String templateId;
    public List<SideNavItem> subNavItems;
}

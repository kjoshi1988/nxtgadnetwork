package models.adNetwork.adsWidget;

import play.db.ebean.Model;

import java.util.List;

/**
 * User: kjoshi
 * Date: 06/11/14
 * Time: 3:08 PM
 */
public class AdDashBoard{
    public int columnCount;
    public int rowCount;
    public String defaultBgColor;
    public List<Category> categories;
    public List<Discount> discounts;
    public String keyword;
    public String priceDrop;
    public String heading = "SponsoredProducts";

//    public static Finder<String,AdDashBoard> find = new Finder<String, AdDashBoard>(String.class,AdDashBoard.class);

}

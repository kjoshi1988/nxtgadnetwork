package models.adNetwork.adsWidget;

import org.json.JSONObject;
import play.db.ebean.Model;

import javax.persistence.Id;
import java.util.HashMap;
import java.util.Map;

/**
 * User: kjoshi
 * Date: 06/11/14
 * Time: 3:07 PM
 */
public class Category extends Model{
    public String name;

    @Id
    public long node;

    public static Finder<String,Category> find = new Finder<String, Category>(String.class,Category.class);

    @Override
    public String toString() {
        Map map = new HashMap();
        map.put("name",name);
        map.put("node",node);
        return new JSONObject(map).toString();
    }
}

package models.adNetwork.adsWidget;

/**
 * User: kjoshi
 * Date: 07/01/15
 * Time: 6:46 PM
 */
public class AdStyle {
    private String fontFamily;
    private String priceStyle;
    private String cardStyle;
    private String headerStyle;
    private String titleStyle;
    private String sellerStyle;

    public static enum AdStyleEnum {
        fontFamily,
        priceStyle,
        cardStyle,
        titleStyle,
        sellerStyle,
        headerStyle;
    }

    public String getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    public String getPriceStyle() {
        return priceStyle;
    }

    public void setPriceStyle(String priceStyle) {
        this.priceStyle = priceStyle;
    }

    public String getCardStyle() {
        return cardStyle;
    }

    public void setCardStyle(String cardStyle) {
        this.cardStyle = cardStyle;
    }

    public String getHeaderStyle() {
        return headerStyle;
    }

    public void setHeaderStyle(String headerStyle) {
        this.headerStyle = headerStyle;
    }

    public String getTitleStyle() {
        return titleStyle;
    }

    public void setTitleStyle(String titleStyle) {
        this.titleStyle = titleStyle;
    }

    public String getSellerStyle() {
        return sellerStyle;
    }

    public void setSellerStyle(String sellerStyle) {
        this.sellerStyle = sellerStyle;
    }
}

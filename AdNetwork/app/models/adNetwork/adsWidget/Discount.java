package models.adNetwork.adsWidget;

import org.json.JSONObject;
import play.db.ebean.Model;

import java.util.HashMap;
import java.util.Map;

/**
 * User: kjoshi
 * Date: 06/11/14
 * Time: 3:07 PM
 */
public class Discount extends Model{
    public String label;
    public String value;

    public static Finder<String,Discount> find = new Finder<String, Discount>(String.class,Discount.class);

    @Override
    public String toString() {
        Map map = new HashMap();
        map.put("label", label);
        map.put("value", value);
        return new JSONObject(map).toString();
    }
}

package models.adNetwork.product;

public class Product {
	private String name;
	private String price;
	private String sellerDispTxt;
	private String imgSrc;
	private String clickURL;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	
	public String getSellerDispTxt() {
		return sellerDispTxt;
	}
	public void setSellerDispTxt(String sellerDispTxt) {
		this.sellerDispTxt = sellerDispTxt;
	}
	public String getImgSrc() {
		return imgSrc;
	}
	public void setImgSrc(String imgSrc) {
		this.imgSrc = imgSrc;
	}
	public String getClickURL() {
		return clickURL;
	}
	public void setClickURL(String clickURL) {
		this.clickURL = clickURL;
	}
	@Override
	public String toString() {
		return "Product [name=" + name + ", price=" + price
				+ ", sellerDispTxt=" + sellerDispTxt + ", imgSrc=" + imgSrc
				+ ", clickURL=" + clickURL + "]";
	}
	
	
}

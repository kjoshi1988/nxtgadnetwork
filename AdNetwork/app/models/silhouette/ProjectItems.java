package models.silhouette;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * User: kjoshi
 * Date: 23/12/14
 * Time: 3:50 PM
 */
@Entity
public class ProjectItems extends Model{
    @Constraints.Required
    public String title;

    @Id
    public String id;
    public String owner;
    public String aboutUs;
    public String channelName;
    public String twitterPage;
    public String fbPage;
    public String gPlusPage;
    public String spriteUrl;

    public static Finder<String,ProjectItems> find = new Finder<String, ProjectItems>(String.class, ProjectItems.class);

    public static List<ProjectItems> findAll(){
        return find.all();
    }

    @Override
    public void save() {
        super.save();
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(System.getProperty("user.dir")+"/conf/testData/silhouette/quickLauncher.yml");
            new org.yaml.snakeyaml.Yaml().dump(ProjectItems.findAll(), fileWriter);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try{
                if(fileWriter!=null) fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

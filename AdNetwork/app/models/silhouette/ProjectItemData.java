package models.silhouette;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.JsonObject;

import play.db.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: kjoshi
 * Date: 29/12/14
 * Time: 2:52 PM
 */
@Entity
public class ProjectItemData extends Model {
    @Id
    public String id;
    @Column(length=2048)
    public String bodyCss;
    @Column(length=2048)
    public String headerCss;
    @Column(length=2048)
    public String logoCss;
    @Column(length=2048)
    public String searchCss;
    @Column(length=2048)
    public String categoriesCss;
    @Column(length=2048)
    public String nlaCss;
    @Column(length=2048)
    public String productCss;
    @Column(length=2048)
    public String p_listCss;
    @Column(length=2048)
    public String related_searchCss;
    @Column(length=2048)
    public String footerCss;


    public static Finder<String,ProjectItemData> find = new Finder<String, ProjectItemData>(String.class, ProjectItemData.class);

    public static List<ProjectItemData> findAll(){
        return find.all();
    }

    public static ProjectItemData loadDefault(String id){
        ProjectItemData projectItemData = new ProjectItemData() ;
        projectItemData.id =id;

        //bodyCss
        JSONObject bodyCss=new JSONObject();
        JSONObject headerCss=new JSONObject();
        JSONObject logoCss=new JSONObject();
        JSONObject searchCss=new JSONObject();
        JSONObject categoriesCss=new JSONObject();
        JSONObject nlaCss=new JSONObject();
        JSONObject p_listCss=new JSONObject();
        JSONObject productCss=new JSONObject();
        
        bodyCss.put("Background CSS","background-color:#fff");
        headerCss.put("Background CSS", "background-color:#fff");
        logoCss.put("URL", "");
        logoCss.put("CSS","background-color:#fff");
        searchCss.put("Search box", "background-color:#fff");
        searchCss.put("Box wrap css", "background-color:#fff");
        searchCss.put("Box css", "background-color:#fff");
        searchCss.put("Button wrap css", "background-color:#fff");
        searchCss.put("Button css", "background-color:#fff");
        searchCss.put("Placeholder", "Search");
        
        categoriesCss.put("Wrap css", "background-color:#fff");
        categoriesCss.put("Node block css", "background-color:#fff");
        categoriesCss.put("Node css", "background-color:#fff");
        categoriesCss.put("Submenu container", "background-color:#fff");
        categoriesCss.put("Submenu item", "background-color:#fff");
        categoriesCss.put("Submenu item hover", "background-color:#fff");
        categoriesCss.put("Submenu link", "background-color:#fff");
        categoriesCss.put("Submenu link hover", "background-color:#fff");
        JSONArray categories = new JSONArray();
        JSONObject cat1 = new JSONObject();
        cat1.put("name","All");
        cat1.put("node","-1");
        categories.put(cat1);
        categoriesCss.put("categories", categories);

        
        nlaCss.put("Text", "Refine");
        nlaCss.put("Position", "horizontal");
        nlaCss.put("Wrap css", "background-color:#fff");
        nlaCss.put("Refine text css", "background-color:#fff");
        nlaCss.put("Pagination info css", "background-color:#fff");
        nlaCss.put("Sub nla container", "background-color:#fff");
        nlaCss.put("Sub nla item", "background-color:#fff");
        nlaCss.put("Sub nla item hover", "background-color:#fff");
        
        p_listCss.put("Per Row Count", "6");
        p_listCss.put("Products Count", "24");
        p_listCss.put("Display type", "Grid");
        p_listCss.put("Pagination", "true");
        p_listCss.put("Margin", "20px");
        p_listCss.put("Product Container Css", "background-color:#fff");
        
        
        productCss.put("Title length", "40");
        productCss.put("Image css", "background-color:#fff");
        productCss.put("Title css", "background-color:#fff");
        productCss.put("Price css", "background-color:#fff");
        productCss.put("Seller name css", "background-color:#fff");
        productCss.put("Rating blank star css", "background-color:#fff");
        productCss.put("Rating full star css", "background-color:#fff");
        productCss.put("Rating half star css", "background-color:#fff");
        
        
        
        
        //bodyCss.put("background-color", "#ffffff");
        projectItemData.bodyCss = bodyCss.toString();
        projectItemData.logoCss=logoCss.toString();
        projectItemData.headerCss = headerCss.toString();
        projectItemData.searchCss = searchCss.toString();
        projectItemData.categoriesCss = categoriesCss.toString();
        projectItemData.nlaCss = nlaCss.toString();
        projectItemData.p_listCss=p_listCss.toString();
        projectItemData.productCss=productCss.toString();


        //footerCss
        JSONObject footerCss = new JSONObject();
        footerCss.put("Nav css", "background-color:#fff");
        footerCss.put("Disclaimer wrap", "background-color:#fff");
        footerCss.put("Price dics container", "background-color:#fff");
        footerCss.put("Facebook icon", "background-color:#fff");
        footerCss.put("Twitter icon", "background-color:#fff");
        footerCss.put("Google plus icon", "background-color:#fff");
        footerCss.put("Footer pb logo", "background-color:#fff");
       
        projectItemData.footerCss = footerCss.toString();



        return projectItemData;
    }

    @Override
    public void save() {
        super.save();
        saveToFile();
    }
    @Override
    public void update(){
    	super.update();
    	saveToFile();
    }
    public void saveToFile(){
    	FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(System.getProperty("user.dir")+"/conf/testData/silhouette/dashBoard.yml");
            new org.yaml.snakeyaml.Yaml().dump(ProjectItemData.findAll(), fileWriter);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try{
                if(fileWriter!=null) fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

package models.common.sideNav;

import controllers.adNetwork.util.Views;

import java.util.List;

/**
 * User: kjoshi
 * Date: 14/11/14
 * Time: 4:06 PM
 */
public class SideNavItem {
    public String label;
    public int sno;
    public String viewAction;
    public String click;
    public String event;
    public List<SideNavItem> subNavItems;
}

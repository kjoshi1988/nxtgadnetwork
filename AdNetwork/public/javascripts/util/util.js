(function(){
    String.prototype.trim = String.prototype.trim || function(){return this.replace(/^\s*\s|\s*\s$/ig,"")};
    String.prototype.format = String.prototype.format || (function(){
        function _getIndex(i, maxOffset) {
            var _tmp = "";
            while (i < maxOffset) {
                var _char = this.charAt(i++),_isHash = (_char === "#");
                _tmp += (_isHash ? "" : _char);
                if(_char === "}") break;
                else if (_char === "#" || _char === "{" || (parseInt(_char)+"" == "NaN") || (i == maxOffset && _char != "}")) return [_isHash ? (i - 1) : i, -1, _tmp];
            }
            return [i,parseInt(_tmp),_tmp];
        }
        return function(fmtArray){
            var _tmp = this+"";
            fmtArray = Array.prototype.slice.call(arguments);
            if (fmtArray && fmtArray.length > 0) {
                var _length = this.length;
                if (_length > 2) {
                    var i = 0; _tmp='';
                    while (i < _length) {
                        var _currentChar = this.charAt(i++);
                        if (_currentChar === "#" && this.charAt(i) === "{") {
                            if (++i >= _length) return _tmp + "#{";
                            var indexArr = _getIndex.call(this, i, _length),fmtIndex = indexArr[1];i = indexArr[0];
                            _tmp += (fmtIndex !="NaN" && fmtIndex != -1 && fmtIndex < fmtArray.length) ? fmtArray[fmtIndex] : ("#{" + indexArr[2]);
                        } else _tmp += _currentChar;
                    }
                }
            }
            return _tmp;
        };
    })();
    Array.prototype.indexOf = Array.prototype.indexOf || function(ele){
        for (var i = 0; i < this.length; i++)
            if (this[i] == ele)
                return i;
        return -1;
    };
})();
var AppConstants = (function () {
    function createInstance() {
        var _this = this;
        this.getAllAdUnitsUrl = function (sellerid, domain) {
            return jsRoutes.controllers.adNetwork.dashboard.MerchantDataController.getAllAdunits(sellerid, domain).url;
        };
        this.getProductsUrl = function (keyword, node, row, column) {
            return jsRoutes.controllers.adNetwork.dashboard.AdsDashBoardController.getProducts(keyword, node, row, column).url;
        };
        this.createMerchantDataUrl = function () {
            return jsRoutes.controllers.adNetwork.dashboard.MerchantDataController.insert().url;
        };
        this.updateMerchantDataUrl = function () {
            return jsRoutes.controllers.adNetwork.dashboard.MerchantDataController.update().url;
        };
        this.deleteMerchantDataUrl = function (adUnitId) {
            return jsRoutes.controllers.adNetwork.dashboard.MerchantDataController.delete(adUnitId).url;
        };
        this.generateAccessTokenUrl = function () {
            return jsRoutes.controllers.adNetwork.dashboard.AccessManagementController.generateAccessToken().url;
        };
        this.getIframeUrl = function (keyword, node, row, column) {
            return jsRoutes.controllers.adNetwork.wizeAds.WizeAdsController.getIframeCode(keyword, node, row, column).url;
        };
        this.getPdfUrl = function (adId) {
            return jsRoutes.controllers.adNetwork.wizeAds.WizeAdsController.generatePdf(adId).url;
        };
        this.defaultCategoryUrl = jsRoutes.controllers.common.api.BizObj.loadCategories().url;
        this.adtypes = [
            {label: "XML", value: 0},
            {label: "JSON", value: 1},
            {label: "HTML", value: 2}
        ];
        this.modetypes = [
            {label: "URL", value: 0},
            {label: "SCRIPT", value: 1}
        ];
        this.layoutTypes = [
            {label: "VERTICAL", value: 0},
            {label: "HORIZONTAL", value: 1}
        ];
        this.$viewUnit = $('#view-adunit');
        this.$createUnit = $('#create-adunit');
        this.getBaseurl = function () {
            var pathArray = window.location.href.split('/');
            var protocol = pathArray[0];
            var host = pathArray[2];
            var url = protocol + '//' + host;
            return url;
        };
        this.CONSTANTS = {
            VIEW_AD_UNITS : {
                ID: "VIEW_AD_UNITS",
                templateUrl: jsRoutes.controllers.Assets.at('angularTemplate/adNtwrk/viewAdUnit.tmp.html').url
            },
            CREATE_NEW_AD:{
                ID:"CREATE_NEW_AD",
                templateUrl: jsRoutes.controllers.Assets.at('angularTemplate/adNtwrk/createAdUnit.tmp.html').url
            }
        };
    }

    var instance;
    return {
        name: "AppConstants",
        getInstance: function () {
            if (instance == undefined)
                instance = new createInstance();
            return instance;
        }
    };
})();

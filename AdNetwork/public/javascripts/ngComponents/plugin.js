(function(win,$){
    var dataDiv = document.createElement("div");
    $(dataDiv).css({position:"absolute",zIndex:"10000000000",display:"none"}).html("<ul></ul>");
    var _isTabletDevice = /Android|iPhone|iPad/i.test(navigator.userAgent);
    var _isWebkit = $.browser.webkit;
    var _isSafari = _isWebkit && !(/chrome/.test(navigator.userAgent.toLowerCase()));
    var mouseEv = _isTabletDevice ? "click" : "click";
    function updatePosition(ele){
        var offset = $(this).offset();
        offset.top += this.offsetHeight;
        $(ele || this._dataDiv).css({left:offset.left,top:offset.top});
    }
    (function(){
        var Cache = (function () {
            var cacheList = {};
            return {
                getItem: function (key) {return cacheList.hasOwnProperty(key) ? cacheList[key] : undefined},
                addItem: function (key, item) {cacheList[key] = item},
                clearItem: function (key) {delete cacheList[key]},
                saveToCache: function (cacheKey,expireTimeInMilliSeconds) {
                    if (win.sessionStorage) {
                        if(JSON.stringify && expireTimeInMilliSeconds && expireTimeInMilliSeconds != -1){
                            var timeStamp = new Date().getTime();
                            win.sessionStorage.setItem(cacheKey, JSON.stringify({cacheList: cacheList, expires : timeStamp + expireTimeInMilliSeconds, timeStamp: timeStamp}));
                        }
                    }else Console.log("Local Caching not supported!!");
                },
                loadFromLocalCache : function(cacheKey, expireTimeInMilliSeconds){
                    if (win.sessionStorage) {
                        var data = $.parseJSON(win.sessionStorage.getItem(cacheKey));
                        if(data && data.expires && (data.expires == -1 || ( data.expires > new Date().getTime() && (!expireTimeInMilliSeconds || (data.timeStamp + expireTimeInMilliSeconds) > new Date().getTime()))))
                            data = data.cacheList;
                        cacheList = $.extend(data,cacheList);
                        Cache.__loaded = true;
                    }else Console.log("Local Caching not supported!!");
                }
            }
        })();
        var listTmp = "<li class='#{0}' _sno='#{1}'>#{2}</li>";
        var updateSelectedIndex = function(index,data,label,displayHandler){
            this.value = $("<span>" + (index == -1 ? this._typedValue : (displayHandler ? displayHandler(data,index) : data[index][label])) + "</span>").text();
            return index;
        };
        var checkForUserAction = function(ev){
            ev = ev || win.event;
            var opts = ev.data.opts;
            var currPointer, data, maxCount, listChildren;
            switch (ev.keyCode) {
                case 27:       //esc
                    hideList.call(this);
                    break;
                case 37:       //left
                    break;
                case 39:       //right
                    break;
                case 38:       //up
                    currPointer = this.pointer - 1, data = this._data, maxCount = data.length, listChildren = this._dataDiv.children[0].children;
                    currPointer = updateSelectedIndex.call(this, currPointer == -2 ? (maxCount - 1) : currPointer, data, opts.label, opts.displayHandler);
                    $(listChildren).removeClass(opts.selectedClass);
                    if (currPointer != -1) $(listChildren[currPointer]).addClass(opts.selectedClass);
                    this.pointer = currPointer;
                    ev.stopPropagation();
                    break;
                case 40:       //down
                    currPointer = this.pointer + 1, data = this._data, maxCount = data.length, listChildren = this._dataDiv.children[0].children;
                    currPointer = updateSelectedIndex.call(this, currPointer == maxCount ? -1 : currPointer, data, opts.label, opts.displayHandler);
                    $(listChildren).removeClass(opts.selectedClass);
                    if (currPointer != -1) $(listChildren[currPointer]).addClass(opts.selectedClass);
                    this.pointer = currPointer;
                    ev.stopPropagation();
            }
        };
        var showResults = function(ev){
            ev = ev || win.event;
            var opts = ev.data.opts;
            var oldValue = this.value.trim(),_this;
            var dss = (getCookie("dss") == "true");
            var forceShowSuggestion = !!ev.data._fromHideSuggestion;
            if(ev.keyCode && ev.keyCode == 13){     //enter
                if(opts.selectionHandler && this.suggestionListShown && this.pointer != -1) {
                    $(this).trigger("enterSelection",ev);
                    opts.selectionHandler.call(this,this.value,this.pointer,this._data);
                }
                hideList.call(this);
            } else if(ev.keyCode && ev.keyCode == 27){  //esc
                _this = this;
                setTimeout(function(){_this.value = oldValue},0);
                ev.stopPropagation();
            } else if (!ev.keyCode || ev.keyCode != 9){
                _this = this;
                if(dss && opts.showHideOptionEnabled)
                    showList.apply(_this,[undefined, opts, undefined]);
                else
                    setTimeout(function () {
                        var searchValue = _this.value.trim();
                        var req = function(){
                            var opts = ev.data.opts;
                            if (searchValue.length < opts.minCharToFetch) {
                                hideList.call(_this);
                            } else if ((searchValue !== oldValue || forceShowSuggestion) && searchValue.length >= opts.minCharToFetch) {
                                var _data = Cache.getItem(searchValue);
                                if (_data) {
                                    showList.apply(_this,[_data,opts,searchValue]);
                                } else {
                                    var _reqIndex = _this._reqArr.length;
                                    _this._reqInProgress = true;
                                    _this._reqArr.push($.ajax({
                                        url: opts.parseUrl ? opts.parseUrl(opts.dataUrl,searchValue) : opts.dataUrl,
                                        success: function (data) {
                                            if(_this._reqArr && _this._reqArr.length > 0 && _reqIndex < _this._reqArr.length)
                                                _this._reqArr.splice(_reqIndex,1);
                                            if (opts.dataFetchCallback)                    //data-format:[{data:'data1',value:'value1'},{data:'data2',value:'value2'}]
                                                data = opts.dataFetchCallback(data);
                                            if(data.length == 0) {
                                                hideList.call(_this);
                                                return;
                                            }
                                            Cache.addItem(searchValue,data);
                                            if(searchValue == _this.value.trim())
                                                showList.apply(_this,[data,opts,searchValue]);
                                        },
                                        failure:function(){
                                            console.log("[AUTO_SUGGEST]Error fetching data")
                                        },
                                        complete:function(){
                                            _this._reqInProgress = false;
                                            _this._nxtReq && _this._nxtReq.length > 0 && _this._nxtReq.pop()();
                                        }
                                    }));
                                }
                            }
                        };
                        _this._timeOut && clearTimeout(_this._timeOut);
                        _this._timeOut = setTimeout(function () {
                            if(opts.queueDataFetchRequest && _this._reqInProgress)
                                _this._nxtReq = [req];
                            else req();
                        }, opts.pauseDuration);
                    }, _isTabletDevice ? 10 : 0);
            }
        };
        var showList = function(data,opts,searchStr){
            updatePosition.call(this);
            var listContainer = this._dataDiv.children[0];
            if (getCookie("dss") != "true" || !opts.showHideOptionEnabled) {
                this._data = data;
                this._typedValue = searchStr;
                var data_list = "";
                if(opts.showHideOptionEnabled)
                    data_list += listTmp.format("suggesterHideShow suggesterOptsShow", -1, "<span>" + opts.showHideOptionHideText + "</span>");
                var _listTmp = listTmp.format(opts.suggestionItemClass);
                var _highLightTmp = "<span class='" + opts.highlightClass + "'>" + searchStr + "</span>";
                for (var i = 0; i < Math.min(data.length,opts.maxItems) ; i++) {
                    var value = data[i][opts.label];
                    if(opts.highlightSearch)
                        value = value.replace(new RegExp(searchStr,"g"),_highLightTmp);
                    data_list += _listTmp.format("",i,value);
                }
                this.pointer = -1;
                this.suggestionListShown = true;
                listContainer.innerHTML = data_list;
                if (_isTabletDevice) $(listContainer).children("li." + opts.suggestionItemClass).bind(mouseEv + " touchstart", this._suggestFn);
            } else
                listContainer.innerHTML = listTmp.format("suggesterHideShow suggesterOptsHide", -1, "<span>" + opts.showHideOptionShowText + "</span>");
            $(this._dataDiv).css("display","block");
        };
        var hideList = function(delay){
            this.pointer = -1;
            this.suggestionListShown = false;
            if(!delay) hideDataDiv.call(this); else setTimeout(hideDataDiv.bind(this), delay);
        };
        var hideDataDiv = function(){$(this._dataDiv).hide()};
        var keyEv = ($.browser.webkit || $.browser.msie) ? "keydown" : "keypress";
        var suggesterController = function(ev){
            if(!!this._autoSuggesterDisabled) return;
            switch (ev.type) {
                case "keyup":
                    if(this.suggestionListShown) checkForUserAction.call(this, ev);
                    break;
                case keyEv :
                    showResults.call(this, ev);
                    break;
                case "blur" :
                    if(this.suggestionListShown) hideList.call(this,800);
            }
        };
        $.fn.autoSuggester = function(opts){
            opts = $.extend({
                dataUrl: "",
                parseUrl: function(url){return url},
                dataFetchCallback: undefined,
                selectionHandler: undefined,
                displayHandler: undefined,
                caching: true,
                label: "value",
                minCharToFetch: 4,
                queueDataFetchRequest: true,
                maxItems: 10,
                highlightSearch: true,
                showHideOptionEnabled: false,
                showHideOptionHideText: "hide search suggestions",
                showHideOptionShowText: "show search suggestions...",
                highlightClass: "suggesterItemHighlight",
                selectedClass: "suggesterItemOver",
                pauseDuration: 250,
                cacheExpiration: -1,
                suggestionContainerClass: "suggesterList",
                suggestionItemClass: "suggesterItem"
            }, opts);
            if(opts.caching && !Cache.__loaded && opts.cacheExpiration != -1){
                Cache.loadFromLocalCache("__autoSuggester",opts.cacheExpiration);
                $(win).bind(_isTabletDevice ? "unload" : "beforeunload", function () {
                    Cache.saveToCache("__autoSuggester",opts.cacheExpiration);
                });
            }
            return this.each(function(){
                if (!this._init){
                    var _dataDiv = dataDiv.cloneNode(true), _this = this,$this = $(_this).attr("autocomplete","off");
                    this._init=true;
                    this._dataDiv = _dataDiv;
                    this._reqArr = [];
                    $this.bind(keyEv + " keyup blur", {opts: opts}, suggesterController);
                    var ul = $(_dataDiv).children("ul").attr({"class": opts.suggestionContainerClass});
                    var _fn = function (ev) {
                        if(_this.suggestionListShown || ev.type == "click"){
                            $(this.parentNode.children).removeClass(opts.selectedClass);
                            switch (ev.type) {
                                case "mouseover":
                                    $(this).addClass(opts.selectedClass);
                                    _this.pointer = parseInt(this.getAttribute("_sno"));
                                    break;
                                case "click":
                                    $(this).addClass(opts.selectedClass);
                                    var data = _this._data, selectedIndex = parseInt(this.getAttribute("_sno"));
                                    updateSelectedIndex.call(_this, selectedIndex, data, opts.label, opts.displayHandler);
                                    if(opts.selectionHandler) {
                                        $(this).trigger("clickSelection",ev);
                                        opts.selectionHandler.call(_this,_this.value,selectedIndex,_this._data);
                                    }
                                    hideList.call(_this);
                                    _this.focus();
                                case "mouseout":
                                    _this.pointer = -1;
                            }
                        }
                    };
                    if(!_isTabletDevice)
                        ul.on("mouseover mouseout " + mouseEv, "li." + opts.suggestionItemClass, _fn);
                    else this._suggestFn = _fn;
                    if(opts.showHideOptionEnabled) $(_dataDiv).on("click","li.suggesterHideShow > span",function(){
                        var dss = getCookie("dss") == "true";
                        setCookie("dss", !dss);
                        if (dss) {
                            showResults.call(_this,{data:{opts: opts, _fromHideSuggestion:true}});
                        } else {
                            hideList.call(_this);
                        }
                    });
                    $(win).click(function(){hideList.call(_this)}).bind("resize",updatePosition.bind(_this));
                    $("body").append(_dataDiv);
                }
            });
        };
        $.fn.autoSuggesterShown = function(){return this[0].suggestionListShown && this[0].pointer != undefined && this[0].pointer != -1};
        $.fn.disableAutoSuggest = function (disable) {
            this.each(function(){
                if(this._init) this._autoSuggesterDisabled = disable;
            });
        };
        $.fn.stopAutoSuggester = function(){
            this.each(function(){
                $(this).blur().focus();
                if(this._reqArr && this._reqArr.length > 0){
                    for (var i = 0; i < this._reqArr.length; i++) {
                        var _req = this._reqArr[i];
                        if(_req && _req.abort && _req.readystate != 4) _req.abort();
                    }
                    this._reqArr = [];
                }
                this._nxtReq = [];
                this._timeOut && clearTimeout(this._timeOut);
            });
        };
    })();

    (function(){
        var tmp = "<li class='#{0}' _sno='#{1}' _value='#{2}'>#{3}</li>";
        function listController(ev){
            if(this._opts.disabled) return;
            switch(ev.type){
                case "click":
                    var _suggestionListShown = this._suggestionListShown ;
                    $(window).click();
                    (_suggestionListShown ? hideList : showList).call(this);
                    $(this._focusEle).focus();
                    ev.stopPropagation();
                    ev.cancelBubble = true;
                    break;
            }
        }
        function focusController(ev) {
            if(this._opts.disabled) return;
            switch (ev.type) {
                case "keydown":
                    keyPressEvent.call(this, ev);
                    break;
                case "focus":
                    this._focused = true;
                    $(this).addClass(this._opts.listContainerFocusedClass);
                    if(this._opts.openOnFocus && !this._dontOpenDropList && !this._suggestionListShown)  showList.call(this);
                    this._dontOpenDropList = false;
                    break;
                case "blur":
                    this._focused = false;
                    hideList.call(this, this._suggestionListShown ? 400 : 0);
            }
        }
        function keyPressEvent(ev){
//            var customEv = $.Event("keydown",{keyCode:ev.keyCode});
            var code = ev.keyCode;
            if(!!code){
                var opts = this._opts, currPointer, data = opts.data, maxCount = data.length, listChildren = this._dataDiv.children[0].children;
                switch(code){
                    case 9:       //tab
                        this._focused = false;
                    case 13:       //enter
                        this._focused = false;
                        if(data != undefined && this._oldSelectedIndex != undefined && data.length > this._oldSelectedIndex &&  this._oldSelectedIndex != -1){
                            var currentIndex =  this._selectedIndex;
                            this._selectedIndex = this._oldSelectedIndex;
                            updateSelectedIndex.call(this, ev, currentIndex, data[currentIndex][opts.valueField], opts, data);
                        }
                        hideList.call(this);
                        this._dontOpenDropList = true;
                        break;
                    case 27:       //esc
                        this._focused = false;
                        if(code == 27 && data != undefined && this._oldSelectedIndex != undefined && data.length > this._oldSelectedIndex &&  this._oldSelectedIndex != -1 ){
                            updateSelectedIndex.call(this, ev, this._oldSelectedIndex, data[this._oldSelectedIndex][opts.valueField], opts, data, true);
                            $(listChildren).removeClass(opts.listItemSelectedClass);
                            $(listChildren[this._oldSelectedIndex]).addClass(opts.listItemSelectedClass);
                            this.pointer = this._oldSelectedIndex;
                        }
                        hideList.call(this);
                        this._dontOpenDropList = true;
                        $(this._focusEle).focus();
                        ev.stopPropagation();
                        ev.preventDefault();
                        break;
                    case 38:       //up
                        currPointer = this._suggestionListShown ? this.pointer : (this._selectedIndex || 0);
                        currPointer = --currPointer < 0 ? (maxCount - 1) : currPointer;
                        $(listChildren).removeClass(opts.listItemSelectedClass);
                        $(listChildren[currPointer]).addClass(opts.listItemSelectedClass);
                        this.pointer = currPointer;
                        ev.stopPropagation();
                        ev.preventDefault();
                        updateSelectedIndex.call(this, ev, currPointer, data[currPointer][opts.valueField], opts, data,this._suggestionListShown);
                        break;
                    case 40:       //down
                        currPointer = (this._suggestionListShown && this.pointer) ? this.pointer : (this._selectedIndex == undefined ? -1 : this._selectedIndex) , listChildren = this._dataDiv.children[0].children;
                        currPointer = (++currPointer) % maxCount;
                        $(listChildren).removeClass(opts.listItemSelectedClass);
                        $(listChildren[currPointer]).addClass(opts.listItemSelectedClass);
                        this.pointer = currPointer;
                        ev.stopPropagation();
                        ev.preventDefault();
                        updateSelectedIndex.call(this, ev, currPointer, data[currPointer][opts.valueField], opts, data,this._suggestionListShown);
                }
                $(this).trigger(ev);
            }
        }
        function showList(){
            updatePosition.call(this);
            this._suggestionListShown = true;
            this._oldSelectedIndex = this._selectedIndex;
            $(this._dataDiv).show();
        }
        function hideList(delay){
            if(!delay) hideDataDiv.call(this); else setTimeout(hideDataDiv.bind(this), delay);
        }
        function hideDataDiv(){
            if(!this._focused) {
                $(this).removeClass(this._opts.listContainerFocusedClass);
                this._suggestionListShown = false;
                this._oldSelectedIndex = -1;
                $(this._dataDiv).hide();
            }
        }
        function updateSelectedIndex(ev, selectedIndex,selectedValue,opts,data,doNotFireChange){
            var oldSelectedIndex = this._selectedIndex;
            this._selectInputTxt.innerHTML = data[selectedIndex][opts.labelField];
            this._selectedIndex = selectedIndex;
            if(!doNotFireChange && (opts.forceFireChange || (oldSelectedIndex != selectedIndex && opts && opts.onChangeHandler))) {
                opts.onChangeHandler(ev, selectedIndex, selectedValue, data, oldSelectedIndex);
                $(this).trigger("change");
            }
        }
        function removeIndex(index,data,opts){
            if(index != undefined && index < data.length){
                if(this._selectedIndex == index){
                    this._selectInputTxt.innerHTML = data[0][opts.labelField];
                    this._selectedIndex = 0;
                } else if(this._selectedIndex > index){
                    this._selectedIndex -= 1;
                }
                var listField = $(this._dataDiv).children("ul")[0];
                var listFieldChildren = listField.children;
                listField.removeChild(listFieldChildren[index]);
                for (var i = index; i < listFieldChildren.length; i++) listFieldChildren[i].setAttribute("_sno",i);
            }
        }
        function populateDropDown(){
            var listData = "", _selectionExist = false,_selectInputTxt = this._selectInputTxt,
                opts = this._opts, data = opts.data, selected = false, _selectedValue = opts.selectedValue,
                listField = $(this._dataDiv).children("ul")[0],_this=this;
            _selectInputTxt.innerHTML = data[0][opts.labelField];
            if(opts.showLoadingAnimation && !!this._loadingImg) this._loadingImg.style.display = "block";
            this._selectedIndex = opts.selectedIndex;
            for (var dataIndex = 0; dataIndex < data.length; dataIndex++) {
                var dataField = data[dataIndex], label = dataField[opts.labelField], value = dataField[opts.valueField];
                selected = !_selectionExist && (dataField[opts.selectedField] || (_selectedValue != undefined && _selectedValue == value) );
                listData += tmp.format(opts.listItemClass + (!!selected ? " "+opts.listItemSelectedClass : ""), dataIndex, value, label);
                if(selected) {
                    this._selectedIndex = dataIndex;
                    _selectionExist = true;
                    _selectInputTxt.innerHTML = label;
                }
            }
            listField.innerHTML = listData;
            if(!selected && this._selectedIndex >= 0 && this._selectedIndex < data.length){
                var listItems = listField.children;
                $(listItems).removeClass(opts.listItemSelectedClass);
                $(listItems[this._selectedIndex]).addClass(opts.listItemSelectedClass);
                _selectInputTxt.innerHTML = data[this._selectedIndex][opts.labelField];
            }
            if (_isTabletDevice) $(listField).children("li." + opts.listItemClass).bind(mouseEv + " touchstart", this._suggestFn);
            if(opts.showLoadingAnimation && !!this._loadingImg) setTimeout(function(){_this._loadingImg.style.display = "none"},200);
        }
        function focusElement(thiz,tabIndex){
            var _drawComboAsFocus = !_isTabletDevice && _isSafari;
            var focusEle = document.createElement(_drawComboAsFocus ? "select" : "input");
            if(!_drawComboAsFocus) focusEle.setAttribute("type","checkbox");
            focusEle.setAttribute("tabindex",tabIndex);
            focusEle.setAttribute("class","checkFocus");
            focusEle.style.cssText = "filter:alpha(opacity=0);opacity:0;position:absolute;left:0;top:0;z-index:-100000;height:1px;width:1px";
            $("body").append(focusEle);
            updatePosition.call(thiz,focusEle);
            return focusEle;
        }
        var defaults = {
            data: [],
            tabIndex: 0,
            labelField: "label",
            valueField: "value",
            selectedField: "selected",
            selectedIndex: 0,
            showLoadingAnimation: true,
            changeLoadingImg: true,
            loadingGif: "//img05.static-nextag.com/imagefiles/psm2_0/loadingDropDown1.gif",
            removeIndex: -1,
            refreshData: true,
            disabled: false,
            openOnFocus: false,
            forceFireChange: false,
            onChangeHandler: function(){},
            listItemSelectedClass: "suggesterItemOver",
            listContainerClass: "suggesterList",
            listContainerFocusedClass: "suggesterListFocused",
            listContainerDisabledClass: "suggesterListDisabled",
            listItemClass: "suggesterItem",
            listItemLoadingClass: "suggesterItemLoadingGif",
            dropBtnClass: "selectBtnIcon",
            inputItemClass: "selectTxt"
        };
        $.fn.dropDown = function(opts){
            return this.each(function(){
                var _opts = $.extend({},this._opts || defaults);
                $.extend(_opts , opts);
                if(!this._init){
                    var _selectBtn = $(this).find("." + _opts.dropBtnClass)[0];
                    var _selectInputTxt = $(this).find("." + _opts.inputItemClass)[0];
                    if(!_selectBtn || !_selectInputTxt) return;
                    this._selectInputTxt = _selectInputTxt;
                    this._selectBtn = _selectBtn;
                    var _dataDiv = dataDiv.cloneNode(true), _this = this, $this = $(_this);
                    this._init = true;
                    this._dataDiv = _dataDiv;
                    this._focusEle = focusElement(this,_opts.tabIndex);
                    $(this._focusEle).bind("keydown focus blur",function(ev){focusController.call(_this,ev)});
                    $this.bind("click",listController).focus(function(){this._focusEle.focus()});
                    var _fn = function(ev){
                        var _opts = _this._opts;
                        $(this.parentNode.children).removeClass(_opts.listItemSelectedClass);
                        switch (ev.type) {
                            case "touchstart":
                                $(this).addClass(_opts.listItemSelectedClass);
                                break;
                            case "mouseover":
                                $(this).addClass(_opts.listItemSelectedClass);
                                _this.pointer = parseInt(this.getAttribute("_sno"));
                                break;
                            case mouseEv:
                                $(this).addClass(_opts.listItemSelectedClass);
                                updateSelectedIndex.call(_this, ev, parseInt(this.getAttribute("_sno")), this.getAttribute("_value"),_opts,_opts.data);
                                hideList.call(_this);
                                _this._dontOpenDropList = true;
                                _this._focusEle.focus();
                                NextagUtils.stopEvent(ev);
                        }
                    };
                    var ul = $(_dataDiv).children("ul").attr({"class":_opts.listContainerClass});
                    if(!_isTabletDevice)
                        ul.on("mouseover mousedown " + mouseEv,"li."+_opts.listItemClass,_fn);
                    else this._suggestFn = _fn;
                    $(win).click(function(){if(_this._suggestionListShown) hideList.call(_this)}).bind("resize",function(){
                        updatePosition.call(_this);
                    });
                    $("body").append(_dataDiv);
                }
                if (_opts.showLoadingAnimation && _opts.changeLoadingImg && _opts.loadingGif.trim().length != 0) {
                    if(this._loadingImg) this._selectBtn.removeChild(this._loadingImg);
                    this._loadingImg = document.createElement("img");
                    this._loadingImg.src = _opts.loadingGif;
                    this._loadingImg.style.display = "none";
                    this._loadingImg.className = _opts.listItemLoadingClass;
                    this._selectBtn.appendChild(this._loadingImg);
                    _opts.changeLoadingImg = false;
                }
                if(_opts.disabled)
                    $(this).addClass(_opts.listContainerDisabledClass);
                else
                    $(this).removeClass(_opts.listContainerDisabledClass);
                if(_opts.removeIndex != -1 && _opts.removeIndex < _opts.data.length){
                    removeIndex.call(this,_opts.removeIndex,_opts.data,_opts);
                    _opts.removeIndex = -1;
                    _opts.data.splice(opts.removeIndex,1);
                }
                this._opts = _opts;
                if (_opts.refreshData && _opts.data && _opts.data.length > 0)
                    populateDropDown.call(this);
                else _opts.refreshData = true;
            });
        };
        $.fn.dropDownProp = function(opts){
            if (typeof opts == "string") {
                var _thiz = this[0];
                switch (opts){
                    case "disabled":
                        return _thiz._opts.disabled;
                    case "selectedIndex":
                        return _thiz._selectedIndex;
                    case "data":
                        return _thiz._opts.data;
                }
            }
            return undefined;
        };
        $.fn.hideDropDown = function(){
            var _thiz = this[0];
            $(_thiz).removeClass(_thiz._opts.listContainerFocusedClass);
            _thiz._suggestionListShown = false;
            _thiz._oldSelectedIndex = -1;
            _thiz._focusEle.blur();
            $(_thiz._dataDiv).hide();
        }
    })();

    (function(doc,$){
        function menuItemAction(ev, opts, isRootNode) {
            if (!this._childMenuNodesFetch) {
                this._childMenuNodesFetch = true;
                var children = $(this).children("." + opts.subMenuWrapClass);
                if (children.length > 0) {
                    this._menuChildren = children;
                }
            }
            var hoverClass = isRootNode ? opts.rootNodeItemClass + "Over" : opts.menuItemOverClass;
            var isValidEvent= ev.type == "click" || ev.type=="mouseenter" || ev.type=="mouseover";
            isValidEvent ? $(this).addClass(hoverClass) : $(this).removeClass(hoverClass);
            if (this._menuChildren) {
                if (isValidEvent){
                    this._menuChildren.show();
                    var childWidth = $(this._menuChildren.children()[0]).width();
                    var left = ($(this).offset().left + $(this).width() + childWidth) > $(window).width() ? (-childWidth - 2) : $(this).width();
                    var pos = {top: "0px", left: left + "px"};
                    if (isRootNode)
                        pos = {top: $(this).height() + "px", left: "0px"};
                    this._menuChildren.css(pos);
                } else
                    this._menuChildren.hide();
            }
        }

        $.fn.dropDownMenu = function (opts) {
            opts = $.extend({
                rootNodeItemClass: "menuRootNode",
                menuItemArrowClass: "menuItemArrow",
                menuItemClass: "menuItem",
                menuItemOverClass: "menuItemOver",
                subMenuClass: "subMenu",
                subMenuWrapClass: "subMenuWrap",
                menuItemTitleClass: "menuItemTitle",
                event:"mouseenter mouseleave",
                timeout:1000,
                data : {}
            }, opts);
            this.each(function () {
                var _data = opts.data;
                var timer;
                if(!!_data){
                    for (var itemKey in _data) {
                        var item = _data[itemKey];
                        var ele = doc.getElementById(itemKey);
                        if(ele){
                            var nodes = item["nodes"];
                            if(nodes && nodes.length != 0 ){
                                var subMenuWrapDiv = $(ele).children("." + opts.subMenuWrapClass)[0];
                                var subMenuHtml = getSubMenuHtml(nodes,item["level"]);
                                if(subMenuWrapDiv){
                                    subMenuHtml = $(subMenuHtml);
                                    subMenuWrapDiv.innerHTML = subMenuHtml.html();
                                    $(subMenuWrapDiv).addClass(subMenuHtml.attr("class"));
                                } else
                                    ele.innerHTML += subMenuHtml;
                            }
                        }
                    }
                }

                $(this).on("mouseenter mouseleave", "." + opts.menuItemClass, function (ev) {
                    menuItemAction.call(this,ev,opts);
                });
                $(this).on(opts.event, "." + opts.rootNodeItemClass, function (ev) {
                	var that=this;
                	var event=ev;
                	var timeout=opts.timeout;
                	if(event.type=="click" || event.type=="mouseleave"){
                		menuItemAction.call(that,event,opts,true);
                	}
                	clearTimeout(timer);
                	timer=setTimeout(function(){
                		menuItemAction.call(that,event,opts,true);
                	},timeout);
                });

            });

            function getSubMenuHtml(nodes, level, addArrowItem){
                var menuTmp = "<div class='#{0}'><ul class='#{1}'>#{2}</ul></div>";
                var menuItemTmp = "<li id='#{0}' class='#{1}'>#{2}</li>";
                var menuItemAnchorTmp = "<a href='#{0}' class='#{1}'>#{2}</a>";
                var nodeItemsHtml = "";
                for (var i = 0; i < nodes.length; i++) {
                    var node = nodes[i];
                    var subMenuHtml = "", subMenuNodes;
                    var subMenu = node["subMenu"];
                    if(subMenu && (subMenuNodes = subMenu["nodes"])){
                        subMenuHtml = getSubMenuHtml(subMenuNodes, subMenu["level"],true);
                    }
                    nodeItemsHtml += menuItemTmp.format(node["id"], opts.menuItemClass, menuItemAnchorTmp.format(node["href"], opts.menuItemTitleClass, node["text"]) + subMenuHtml);
                }
                return menuTmp.format(opts.subMenuWrapClass + " menu-level-" + level, opts.subMenuClass, nodeItemsHtml) + (!!addArrowItem ? ("<div class='#{0}'></div>").format(opts.menuItemArrowClass,"") : "");
            }
        }
        $(".mainDiv").on("click",".menuRootNode",function(){
            if(this.fireUrl){
                var channel = _Wize.getGlobalVar('currentChannel') || 'psmus';
                window.location = "/serv/"+channel+"/buyer/Outpdir.jsp?node="+this.id;
                this.fireUrl=false;
            }
            else
                this.fireUrl = true;
        });
    })(document,jQuery);

    window.infiniteScroll = (function(){
        var _init, _evQueue;
        function xhr(url, callBack){
            invokeAjaxCall(url, callBack, false);
            return true;
        }
        function checkForDefaultParams(opts){
            opts.infiniteContainerId = opts.infiniteContainerId || "";
            opts.loadingText = opts.loadingText || "";
            opts.loadingImg = opts.loadingImg || "/webcontent/birt/images/ajax-loader.gif";
            opts.dataUrl = opts.dataUrl || function(){};
            opts.onComplete = opts.onComplete || function(){};
            opts.processResponseTxt = opts.processResponseTxt || function(txt){return txt};
            opts.offsetToFetch = opts.offsetToFetch || "2";
            opts.fetchCountThreshold = opts.fetchCountThreshold || -1;
            return opts;
        }
        return {
            init: function () {
                if (!_init) {
                    var _body = document.body || document.getElementsByTagName("body")[0],_timer;
                    var _scrollFn = function(){
                        !!_timer && clearTimeout(_timer);
                        _timer = setTimeout(function(){
                            var _max = (_body.scrollTop || _body.parentNode.scrollTop) + _body.parentNode.clientHeight;
                            for (var index = 0; index < _evQueue.length; index++)
                                _evQueue[index](_max);
                        },500);
                    };
                    if (window.attachEvent) window.attachEvent("onscroll", _scrollFn); else window.addEventListener("scroll", _scrollFn, false);
                    _init = true;
                }
            },
            bind : function(opts){
                this.init();
                _evQueue = _evQueue || [];
                var _fn = (function(_opts){
                    var ele = document.querySelector(_opts.infiniteContainerId),_fetchCount=0;
                    var _xhr;
                    if(!ele)
                        return null;
                    return function(max){
                        var _maxScrolled = ((ele.offsetHeight || ele.scrollHeight) + ele.offsetTop);
                        if (ele.scrollHeight > ele.offsetHeight) {
                            max = ele.scrollTop + ele.offsetHeight;
                            _maxScrolled = ele.scrollHeight;
                        }
                        var _scrollOffset = parseInt(_opts.offsetToFetch);
                        if(_opts.offsetToFetch.indexOf("%") != -1)
                            _maxScrolled -= _maxScrolled * _scrollOffset / 100;
                        else
                            _maxScrolled -= _scrollOffset;
                        if (max >= _maxScrolled && !ele._reqSent && (opts.fetchCountThreshold < 0 || _fetchCount++ < opts.fetchCountThreshold)) {
                            var loadingImgContainer = document.createElement("div");
                            loadingImgContainer.style.cssText = "text-align:center;clear:both;width:100%;padding: 10px 0";
                            loadingImgContainer.innerHTML = _opts.loadingText + "<img style='display:inline' src='" + _opts.loadingImg + "' />";
                            ele.appendChild(loadingImgContainer);
                            ele._reqSent = true;
                            _xhr = xhr(_opts.dataUrl(), function (responseText) {
                                ele.removeChild(loadingImgContainer);
                                try{
                                    if(!!responseText){
                                        ele.innerHTML += _opts.processResponseTxt(responseText);
                                        _opts.onComplete();
                                    }
                                } catch(e){
                                    console.error("Error processing response text");
                                }
                                _xhr = undefined;
                                ele._reqSent = false;
                            });
                        }
                    };
                })(checkForDefaultParams(opts));
                _fn && _evQueue.push(_fn);
            }
        }
    })();
})(window,jQuery);

App.onAppLoad(function (ngApp) {
    ngApp.factory('adNetworkService', ['$http', '$q', function ($http, $q) {
        function getData(url) {
            return createHttpRequest('get', url).then(handleSuccess, handleError);
        }
        function sendData(url, data) {
            return createHttpRequest('post', url, data).then(handleSuccess, handleError);
        }
        function updateData(url, data) {
            return createHttpRequest('put', url, data).then(handleSuccess, handleError);
        }
        function deleteData(url) {
            return createHttpRequest('delete',url).then(handleSuccess, handleError);
        }
        function createHttpRequest(method, url, data) {
            return $http({
                method: method,
                url: url,
                data: data,
                timeout: 10000
            });
        }
        function handleError() {
            return handleSuccess(JSON.stringify({data:[]}));
        }
        function handleSuccess(response) {
            return response.data;
        }
        return({
            getData: getData,
            sendData: sendData,
            updateData: updateData,
            deleteData:deleteData
        });
    }]);
});

App.onAppLoad(function(ngApp){
    ngApp.factory('messageService',function($rootScope){
		var service = {};
		var messageData={};
		service.sendMessage=function(message,data){
			messageData.currentMessage=message;
			messageData.data=data;
			$rootScope.$broadcast(message,data);
		}
		service.getCurrentMessage=function(){
			return messageData.currentMessage;
		}
		return service;
	});
});
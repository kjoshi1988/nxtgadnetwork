App.onAppLoad(function(ngApp){
    ngApp.factory('templateService',function($rootScope, $compile){
        var _tmpMap = {}, _tmpChangEv = {}, _rootPanel, _loader, _currentTmp, _currentSideNav;
        var _templateTagText = "<right-view template='#{0}' source='#{1}'></right-view>", _stillLoading;
        function _triggerTmpChange(_tmpName, callBack){
            var _evArr = _tmpChangEv[_tmpName];
            var _tmpEle = _tmpMap[_tmpName];
            if (!!_evArr && !!_tmpEle) {
                var _eventObj = {
                    oldTmp : _currentTmp,
                    oldEle: _tmpMap[_currentTmp],
                    newTmp: _tmpName,
                    newEle: _tmpEle
                };
                for (var _i = 0; _i < _evArr.length; _i++)
                    setTimeout((function(index){
                        return function(){_evArr[index](_eventObj)}
                    })(_i),0);
                _currentTmp = _tmpName;
            }
            callBack();
        }
        var showLoader = function () {
            !!_loader && $(_loader).show();
            _currentTmp && _tmpMap[_currentTmp] && $(_tmpMap[_currentTmp]).hide();
            !!_rootPanel && $(_rootPanel).hide();
            _stillLoading = true
        };
        var hideLoader = function () {
            !!_loader && $(_loader).hide();
            _currentTmp && _tmpMap[_currentTmp] && $(_tmpMap[_currentTmp]).show();
            !!_rootPanel && $(_rootPanel).show();
            _stillLoading = false
        };
        return {
            registerRootElements: function (rootPanel, loader) {
                _rootPanel = rootPanel;
                _loader = loader;
            },
            addTemplate: function (tmpName, scope, doNotStopAutoLoader) {
                if(!!_rootPanel && !_stillLoading){
                    showLoader();
                    if(this !== window)
                        _currentSideNav = this;
                    $($("[data-viewaction=\""+tmpName+"\"]").siblings()).removeClass('active');
                    $("[data-viewaction=\""+tmpName+"\"]").addClass('active');
                    var _div = _tmpMap[tmpName];
                    if(!_div){
                        _div = document.createElement("div");
                        _div.setAttribute("data-view-id", tmpName);
                        _div.className = "viewContainer";
                        _tmpMap[tmpName] = _div;
                        _rootPanel.appendChild(_div);
                        _div.innerHTML = _templateTagText.format(tmpName, "source[\""+tmpName+"\"]");
                        $compile(_div.children)(scope);
                    }
                    _triggerTmpChange(tmpName, function(){});
                    !doNotStopAutoLoader && hideLoader();
                }
            },
            changeTemplate: function (tmpName, callBack, doNotStopAutoLoader) {
                var _div = _tmpMap[tmpName];
                if(!!_div && !_stillLoading){
                    showLoader();
                    _triggerTmpChange(tmpName, !doNotStopAutoLoader ? function(){callBack();hideLoader()} : callBack);
                }
            },
            showLoader: showLoader,
            hideLoader: hideLoader,
            addTmpShowEvent: function(tmpName, callBack){
                if(typeof callBack === "function"){
                    var _evArr = _tmpChangEv[tmpName];
                    if(!_evArr){
                        _evArr = [];
                        _tmpChangEv[tmpName] = _evArr;
                    }
                    _evArr.push(callBack);
                }
            },
            removeTmpShowEvent:function(tmpName, callBack){
                if(typeof callBack === "function"){
                    var _evArr = _tmpChangEv[tmpName];
                    if (!!_evArr) {
                        var _len = _evArr.length;
                        for (var i = 0; i < _len; i++) {
                            var _fn = _evArr[i];
                            if (_fn == (callBack + "")) {
                                _evArr.splice(i, 1);
                                i--;
                                _len = _evArr.length;
                            }
                        }
                    }
                }
            }
        };
    });
});

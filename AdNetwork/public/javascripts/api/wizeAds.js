(function (win, doc) {
    var adUrl = "http://{0}/wizeAds/script/products";
    var wizeOnReady = (win["_wizeAds"] && win["_wizeAds"]["onReady"]) || function () {};
    win._wizeAds = (function () {
        function loadAd(adUnit) {
            if (!adUnit.clientId)
                console.error("No client id specified");
            if (!adUnit.query)
                console.error("No query specified");
            var clientId = adUnit.clientId;
            var query = adUnit.query;
            var ele = doc.getElementById(adUnit['container']);
            if (!!ele) {
                ele.style.height = adUnit["height"] || "500px";
                ele.style.width = adUnit["width"] || "100%";
                ele.innerHTML = "<iframe style='width:100%;height:100%;border:none' src='" + getAdUrl(query, clientId, encodeStyle(adUnit)) + "'></iframe>";
            }
        }
        function encodeStyle(adUnit) {
            var fontFamily = adUnit['fontFamily'];
            var titleStyle = adUnit['titleStyle'];
            var priceStyle = adUnit['priceStyle'];
            var sellerStyle = adUnit['sellerStyle'];
            var headerStyle = adUnit['headerStyle'];
            var cardStyle = adUnit['cardStyle'];
            var encodeStyle = [];
            if (!!fontFamily)
                encodeStyle.push(encodeStr("fontFamily") + "@@" + encodeStr(fontFamily));
            if (!!titleStyle)
                encodeStyle.push(encodeStr("titleStyle") + "@@" + encodeStr(titleStyle));
            if (!!priceStyle)
                encodeStyle.push(encodeStr("priceStyle") + "@@" + encodeStr(priceStyle));
            if (!!sellerStyle)
                encodeStyle.push(encodeStr("sellerStyle") + "@@" + encodeStr(sellerStyle));
            if (!!headerStyle)
                encodeStyle.push(encodeStr("headerStyle") + "@@" + encodeStr(headerStyle));
            if (!!cardStyle)
                encodeStyle.push(encodeStr("cardStyle") + "@@" + encodeStr(cardStyle));
            return encodeURIComponent(encodeStyle.join("@@@"));
        }

        function encodeStr(str) {
            var encodedStr = [];
            for (var i = 0; i < str.length; i++) {
                var _char = str.charAt(i);
                var _charCode = _char.charCodeAt(0);
                encodedStr.push((_charCode >= 97 && _charCode <= 122) || (_charCode >= 65 && _charCode <= 90) ? _charCode : _char);
            }
            return encodedStr.join("@");
        }

        function getAdUrl(query, clientId, styles) {
            var _arr = [adUrl];
            _arr.push(query);
            _arr.push(clientId);
            return _arr.join("/") + "?styles=" + styles;
        }

        return {
            loadAds: function () {
                var adUnits = Array.prototype.slice.apply(arguments);
                for (var i = 0; i < adUnits.length; i++) {
                    loadAd(adUnits[i]);
                }
            }
        };
    })();
    wizeOnReady();
})(window, document);
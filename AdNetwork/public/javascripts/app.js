window.App = (function () {
    var _init, _appLoad = [];
    return {
        ngApp: angular.module("app", []),
        init: function () {
            if (!_init) {
                _init = true;
                while(_appLoad.length != 0) _appLoad.splice(0, 1)[0](App.ngApp);
                var mainDiv = document.getElementById("main");
                angular.bootstrap(mainDiv, ['app']);
            }
        },
        onAppLoad: function (_fn) {
            _init && _fn(App.ngApp);
            !_init && _appLoad.push(_fn);
        }
    }
})();
$().ready(function () {
    var _dashBoard = $(".dashboardContent");
    var _dashBoardPanel = $("#panel");
    var _loader = $("#dasboardLoader");
    var _leftNav = $(".leftNav");
    var _win = $(window);
    _win.resize(adjustDashBoard());
    function adjustDashBoard() {
        _dashBoard.height(_win.height() - 102);
        _leftNav.height(_win.height() - 102);
        return adjustDashBoard;
    }

    _win.load(function () {
        setTimeout(function () {
            //_loader.hide();
            _dashBoardPanel.show();
        }, 1000);
    });
    App.init();
});

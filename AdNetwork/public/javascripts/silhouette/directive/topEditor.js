App.onAppLoad(function(ngApp){
  ngApp.directive('topEditor', function(){
    return {
       restrict: 'E',
       templateUrl: jsRoutes.controllers.Assets.at('angularTemplate/silhouette/topEditorTemplate.html').url
    };
  });
});

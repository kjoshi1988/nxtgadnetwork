App.onAppLoad(function (ngApp) {
    ngApp.directive('saveButton', function () {
        return {
            restrict: 'A',
            link: function ($scope, iElm, iAttrs, controller) {
                $scope.input={};
                iElm.on('click', function () {
                    var identifier = iAttrs["identifier"];
                    var combinedValue = {id: identifier, css: {}, data: {}};
                    var appConst = AppConstants.getInstance();
                    $(this).parent().find(".cssEdit").each(function(){
                        var lbl = $(this).prev().html().toLowerCase().replace(/\s/ig,"-");
                        combinedValue.css[appConst.CONST[identifier].CSS_NAMES[lbl]] = $(this).val().replace(/\n/ig,'');
                    });
                    switch (identifier){
                        case appConst.CONST.PRODUCT.ID:
                            combinedValue.data["titleLength"] = $scope.input.titleLength;
                            break;
                        case appConst.CONST.NLA.ID:
                            combinedValue.data["refineText"] = $scope.input.refinetext;
                            break;
                        case appConst.CONST.SEARCH.ID:
                            combinedValue.data["placeholder"] = $scope.input.placeholder;
                            break;
                        case appConst.CONST.PLIST.ID:
                            combinedValue.data["colCount"] = $scope.input.colCount;
                            combinedValue.data["totalCount"] = $scope.input.totalCount;
                            combinedValue.data["showPagination"] = $scope.input.showPagination;
                            combinedValue.data["margin"] = $scope.init.margin;
                            combinedValue.data["displayType"] = $scope.input.displayType;
                            break;
                    }
                    $scope.changedata()(combinedValue);
                });
            }
        };
    });
});

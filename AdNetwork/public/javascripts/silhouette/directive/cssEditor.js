App.onAppLoad(function (ngApp) {
    ngApp.directive('cssEditor', function ($parse, $http, $compile, $templateCache) {
        function linker(scope, element, attrs) {
            scope.$watch('view', function () {
                var key = scope.view.toLowerCase() + "Css";
                if (key !== 'Css') {
//                    var parseKey = AppConstants.getInstance().projectData[key];
//                    if (parseKey)
                    scope.cssprop = AppConstants.getInstance().projectData[key];
                    scope.templateUrl = AppConstants.getInstance().templateMap[scope.view];
                    switch (scope.view) {
                        case 'P_LIST':
                            scope.totalCount = 40;
                            scope.colCount = 8;
                            scope.colCountArr = [];
                            scope.totalCountArr = [];
                            scope.showPagination = false;
                            scope.margin = 0;
                            scope.displayType = "grid";
                            for (var i = 1; i <= scope.totalCount; i++)
                                scope.totalCountArr.push(i);
                            for (i = 1; i <= scope.colCount; i++)
                                scope.colCountArr.push(i);
                            break;
                    }
                }
            });
        }

        return {
            restrict: 'E',
            transclude: true,
            scope: {
                view: "=",
                action: "&",
                changedata: "&",
                showeditor: "&"
            },
            template: '<div ng-include="templateUrl"></div>',
            link: linker
        };
    });
});

App.onAppLoad(function (ngApp) {
    var updateEvent=function(){};
    ngApp.directive('initEdit', function ($rootScope) {
        return {
            restrict: 'A',
            scope: {
                action: "&",
                showeditor: "&",
                identifier: "@"
            },
            link: function (scope, iElm, iAttrs, controller,$rootScope) {
                function updateEventCall(event, data){
                    iElm.val(data.css);
                    updateEvent();
                }
               /* iElm.on('change', function () {
                    scope.isDirty = true;
                });*/
                iElm.on('focus', function () {
                        scope.action()()();
                        scope.showeditor()()(iElm.val().trim(), false, scope.identifier);
                        updateEvent();
                        updateEvent=scope.$on('updateCss', updateEventCall);
                });

            }
        };
    });
});

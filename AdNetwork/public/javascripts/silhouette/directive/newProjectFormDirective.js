App.onAppLoad(function(ngApp){
    ngApp.directive('newProjectForm',function(){
        return{
            restrict:'E',
            replace: true,
            templateUrl:jsRoutes.controllers.Assets.at('angularTemplate/silhouette/createNewProject.html').url
        };
    });
});
App.onAppLoad(function (ngApp) {
    ngApp.directive("categoryItem", function ($http, messageService) {
        var selectedNodes = [];
        return {
            restrict: "E",
            templateUrl: jsRoutes.controllers.Assets.at('angularTemplate/silhouette/categoryItem.html').url,
            link: function (scope, element, attrs) {
                $http({
                    url: "/api/categories",
                    type: "GET"
                }).success(function (data) {
                    scope.catergoryItems = [];
                    for (var i = 0; i < scope.value.length; i++)
                        selectedNodes.push(scope.value[i]["node"]);
                    for (i = 0; i < data.length; i++) {
                        var current = data[i];
                        if (selectedNodes.indexOf(current["node"]) == -1)
                            scope.catergoryItems.push(current);
                    }
                });

                scope.addCategory = function () {
                    scope.value.push(this.cat);
                    selectedNodes.push(this.cat.node);
                    scope.catergoryItems.splice(this.$index, 1);
                    messageService.sendMessage("updateCategory", scope.value);
                };

                scope.$on("updateCategoryInEditor", function(event, data){
                    var index = selectedNodes.indexOf(data.node);
                    if(index != -1){
                        selectedNodes.splice(index,1);
                        scope.catergoryItems.push(data);
                    }
                    if (!scope.$$phase)
                        scope.$digest();
                });
            }
        }
    });
});
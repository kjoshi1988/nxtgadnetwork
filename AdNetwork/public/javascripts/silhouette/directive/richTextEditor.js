App.onAppLoad(function(ngApp){
    ngApp.directive("richTextEditor", function(){
        function exec(doc, cmd, val){
            doc.execCommand(cmd, false, val);
        }
        return{
            restrict:'E',
            replace: true,
            templateUrl:jsRoutes.controllers.Assets.at('angularTemplate/silhouette/richTextEditorTemplate.html').url,
            link : function(scope, element, attrs) {
                var rte = $(element);
                var iframe = rte.find("iframe.editorContent")[0];
                var modelName = attrs.ngModel;
                if(!!iframe){
                    $(iframe).load(function(){
                        iframe.contentEditable = true;
                        var doc = iframe.contentWindow.document || iframe.contentDocument;
                        doc.designMode = "on";
                        rte.on("click" , ".control", function(){
                            iframe.focus();
                            $(this).toggleClass("controlDown");
                            exec(doc, this.getAttribute("data-cmd"), null);
                        });
                        $(doc).focus(function(){
                            $(iframe).addClass("");
                        }).blur(function(){
                            scope[modelName] = doc.body.innerHTML;
                        });
                    });
                }

            }
        };
    });
});
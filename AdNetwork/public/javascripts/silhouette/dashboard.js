window.App = (function () {
    var _init, _appLoad = [];
    return {
        ngApp: angular.module("siloDashboard", []),
        init: function () {
            if (!_init) {
                _init = true;
                while(_appLoad.length != 0) _appLoad.splice(0, 1)[0](App.ngApp);
                var mainDiv = document.getElementById("mainSiloDashboard");
                angular.bootstrap(mainDiv, ['siloDashboard']);
            }
        },
        onAppLoad: function (_fn) {
            _init && _fn(App.ngApp);
            !_init && _appLoad.push(_fn);
        }
    };
})();
$().ready(App.init);
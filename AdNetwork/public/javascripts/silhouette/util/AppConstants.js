var AppConstants = (function () {
    function createInstance() {
        this.templateMap = {
            'BODY': jsRoutes.controllers.Assets.at('angularTemplate/silhouette/bodyTemplate.html').url,
            'HEADER': jsRoutes.controllers.Assets.at('angularTemplate/silhouette/headerTemplate.html').url,
            'LOGO': jsRoutes.controllers.Assets.at('angularTemplate/silhouette/logoTemplate.html').url,
            'SEARCH': jsRoutes.controllers.Assets.at('angularTemplate/silhouette/searchTemplate.html').url,
            'CATEGORIES': jsRoutes.controllers.Assets.at('angularTemplate/silhouette/categoryTemplate.html').url,
            'NLA': jsRoutes.controllers.Assets.at('angularTemplate/silhouette/nlaTemplate.html').url,
            'P_LIST': jsRoutes.controllers.Assets.at('angularTemplate/silhouette/plistTemplate.html').url,
            'PRODUCT': jsRoutes.controllers.Assets.at('angularTemplate/silhouette/productTemplate.html').url,
            'FOOTER': jsRoutes.controllers.Assets.at('angularTemplate/silhouette/footerTemplate.html').url
        };
        this.CONST = {
            FOOTER : {
                ID: "FOOTER",
                CSS_NAMES : {
                    "nav-css":"nav",
                    "disclaimer-wrap":"companyLogoWrap",
                    "price-dics-container":"disclaimer",
                    "facebook-icon":"fb",
                    "twitter-icon":"twitter",
                    "google-plus-icon":"gPlus",
                    "footer-pb-logo":"companyLogo"
                }
            },
            BODY: {
                ID:"BODY",
                CSS_NAMES: {
                    "background-css":"background"
                }
            },
            HEADER: {
                ID:"HEADER",
                CSS_NAMES: {
                    "background-css":"background"
                }
            },
            LOGO: {
                ID:"LOGO",
                CSS_NAMES: {
                    "css":"background"
                }
            },
            SEARCH: {
                ID:"SEARCH",
                CSS_NAMES: {
                    "box-css":"box",
                    "box-wrap-css":"boxWrap",
                    "button-css":"buttonCss",
                    "button-wrap-css":"buttonWrapCss",
                    "search-box":"searchBox"
                }
            },
            PRODUCT : {
                ID: "PRODUCT",
                CSS_NAMES : {
                    "image-css":"img",
                    "title-css":"title",
                    "price-css":"price",
                    "seller-name-css":"sellerName",
                    "rating-blank-star-css":"ratingBlank",
                    "rating-full-star-css":"ratingFull",
                    "rating-half-star-css":"ratingHalf"
                }
            },
            PLIST : {
                ID: "PLIST",
                CSS_NAMES : {
                }
            },
            CATEGORY : {
                ID: "CATEGORY",
                CSS_NAMES : {
                    "node-block-css" : "nodeBlock",
                    "node-css" : "node",
                    "submenu-container" : "subMenu",
                    "submenu-item" : "subMenuItem",
                    "submenu-item-hover" : "subMenuItemHover",
                    "submenu-link" : "subMenuLink",
                    "submenu-link-hover" : "subMenuLinkHover",
                    "wrap-css" : "wrap"
                }
            },
            NLA : {
                ID: "NLA",
                CSS_NAMES : {
                    "page-info-css": "pageInfo",
                    "refine-text-css": "refineText",
                    "sub-nla-container": "subNlaContainer",
                    "sub-nla-item": "subNlaItem",
                    "sub-nla-item-hover": "subNlaItemHover",
                    "wrap-css": "wrap"
                }
            }
        };
        this.defaultTitle = "2.4GHz Wireless Car Rear View Waterproof IP67 Reverse Backup Parking Camera (Backup Camera)";
    }

    var instance;
    return {
        name: "AppConstants",
        getInstance: function () {
            if (instance == undefined)
                instance = new createInstance();
            return instance;
        }
    };
})();

App.onAppLoad(function (ngApp) {
    ngApp.controller('dashBoardController', function (messageService, $scope, adNetworkService) {
        $scope.sendMessage = function (message, data) {
            messageService.sendMessage(message, data);
        }
        $scope.getProjectData = function () {
            var dataPromise = adNetworkService.getData('/silhouette/project/data/' + window._projectId);
            dataPromise.then(function (data) {
                for(var key in data){
                    if(data.hasOwnProperty(key)){
                        data[key] = JSON.parse(data[key]);
                    }
                }
                AppConstants.getInstance().projectData = data;
                messageService.sendMessage("updateProjectData",{});
            });
        }
        $scope.saveChanges=function(event){
            var savedData={};
            $.extend(savedData,AppConstants.getInstance().projectData);
            for(var key in savedData){
                savedData[key]=JSON.stringify(savedData[key]);
            }
            adNetworkService.sendData('/silhouette/project/save/'+window._projectId, savedData);
            alert("Changes saved successfully");
        }
    });
});

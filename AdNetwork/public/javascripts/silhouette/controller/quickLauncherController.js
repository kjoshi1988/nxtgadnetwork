App.onAppLoad(function (ngApp) {
    ngApp.controller("projectListController", function ($scope, $compile, $http) {
        var _createFormInit = false;
        $http.get("/silhouette/projects/list").success(function (data) {
            $scope.projectsList = data;
        });
        $scope.showNewProject = function () {
            !_createFormInit && $("body").append($compile('<new-project-form></new-project-form>')($scope));
            _createFormInit = true;
            $scope.showNewProjectForm = true;
        };
        $scope.resetForm = function () {
            $("#newProjectForm").get(0).reset();
            $scope.showNewProjectForm = false;
        };
        $scope.createNewProject = function ($event) {
//            $("#newProjectForm").get(0).reset();
            var form = $event.target;
            if (!$scope.name || $scope.name.trim().length == 0)
                alert("Please enter the name");
            else {
                var params = "name="+$scope.name +
                    "&owner=kjoshi";
                $http({
                    url:"/silhouette/project/create",
                    method: "POST",
                    data: {
                        name: $scope.name,
                        owner: "kjoshi",
                        channel: $scope.channel,
                        domain: $scope.domain,
                        fbPage: $scope.fbPage,
                        twitterPage: $scope.twitterPage,
                        gPlusPage: $scope.gPlusPage,
                        spriteUrl: $scope.spriteUrl,
                        aboutUs: $scope.aboutUs
                    }
                }).success(function(data){
                    $scope.showNewProjectForm = false;
                    $scope.projectsList.push(data);
                });
            }
        };
    });
});
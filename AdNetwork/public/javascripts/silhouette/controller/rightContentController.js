App.onAppLoad(function (ngApp) {
    ngApp.controller('rightContentController', function ($scope, messageService) {
        $scope.currentView = '';
        $scope.isEditMode = false;
        $scope.cssProperties = [];
        $scope.cssProp = "background:#ffffff";
        $scope.$on('updateRightView', function (event, data) {
            $scope.currentView = data;
            $scope.isEditMode = false;
            $scope.cssProperties = [];
            if (!$scope.$$phase)
                $scope.$digest();
        });
        $scope.initEditMode = function () {
            $scope.isEditMode = true;
            if (!$scope.$$phase)
                $scope.$digest();
        };
        $scope.showCssEditor = function (str, isSaveMode, identifier) {
//            if (isSaveMode)
            $scope.cssProperties = [];
            $scope.identifier = identifier;
            var properties = str.split(';');
            for (var i = 0; i < properties.length; i++) {
                var cssObj = {};
                if(properties[i].length>0){
                    var cssProp = properties[i].split(':');
                    cssObj.key = cssProp[0];
                    cssObj.value = cssProp[1];
                    $scope.cssProperties.push(cssObj);
                }
            }
            if (!$scope.$$phase)
                $scope.$digest();
        };
        $scope.updateCssProp = function(){
            var css='';
            for(var i=0;i<$scope.cssProperties.length;i++){
                var cssObj=$scope.cssProperties[i];
                css+=cssObj.key+":"+cssObj.value+";";
            }
            $scope.isEditMode = false;
            messageService.sendMessage("updateCss", {id:$scope.identifier, css:css});
        };
        $scope.saveCss = function(){

        };
        $scope.changeData = function(data){
            messageService.sendMessage("applyCss", data);
        }
    });
});

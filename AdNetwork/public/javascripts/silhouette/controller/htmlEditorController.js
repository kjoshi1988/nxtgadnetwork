App.onAppLoad(function (ngApp) {
    ngApp.controller("htmlEditorController", function ($scope, messageService) {
        var appConst = AppConstants.getInstance();

//        $scope.totalCount = 40;
//        $scope.productCols = 8;
//        $scope.titleLength = 40;
//        $scope.refineText = "Refine";
//        $scope.products = getProducts($scope.totalCount, $scope.productCols, $scope.titleLength, {});
//        $scope.footer = {css: {}};
//        $scope.nla = {css: {}};
//        $scope.product = {css: {}};
        $scope.loader = true;
        $scope.$on("applyCss", function (event, obj) {
            $scope.loader = true;
            switch (obj.id) {
                case appConst.CONST.PLIST.ID:
                    $scope.totalCount =  obj.data["totalCount"];
                    $scope.productCols =  obj.data["colCount"];
                    $scope.showPagination =  obj.data["showPagination"];
                    $scope.displayType =  obj.data["displayType"];
                    $scope.products = getProducts($scope.totalCount, $scope.productCols, $scope.titleLength, obj.css, true);
                    $scope.pList.css = obj.css;
                    break;
                case appConst.CONST.PRODUCT.ID:
                    $scope.titleLength = obj.data["titleLength"];
                    $scope.products = getProducts($scope.totalCount, $scope.productCols, $scope.titleLength, obj.css);
                    $scope.product.css = obj.css;
                    break;
                case appConst.CONST.FOOTER.ID:
                    $scope.footer.css = obj.css;
                    break;
                case appConst.CONST.NLA.ID:
                    $scope.refineText = obj.data["refineText"];
                    $scope.nla.css = obj.css;
                    break;
                case appConst.CONST.BODY.ID:
                    $scope.body.css=obj.css;
                    break;
                case appConst.CONST.HEADER.ID:
                    $scope.header.css=obj.css;
                    break;
                case appConst.CONST.LOGO.ID:
                    $scope.logo.css=obj.css;
                    break;
                case appConst.CONST.SEARCH.ID:
                    $scope.search.css=obj.css;
                    $scope.placeHolder = obj.data["placeholder"];
                    break;
                case appConst.CONST.CATEGORY.ID:
                    $scope.category.css=obj.css;
            }
            $scope.loader = false;
            if (!$scope.$$phase)
                $scope.$digest();
            saveToAppData($scope);
        });
        $scope.$on("updateCategory", function(event, data){
            $scope.loader = true;
            $scope.categories = data;
            $scope.loader = false;
            if (!$scope.$$phase)
                $scope.$digest();
            saveToAppData($scope);
        });
        $scope.$on("updateProjectData", function(event, data){
            $scope.loader = true;
            if (!$scope.$$phase)
                $scope.$digest();
            var appData = appConst.projectData;
            $scope.logoUrl =  appData["logoCss"]["URL"];
            $scope.categories = appData["categoriesCss"]["categories"];
            $scope.totalCount = appData["p_listCss"]["Products Count"];
            $scope.productCols = appData["p_listCss"]["Per Row Count"];
            $scope.showPagination = appData["p_listCss"]["Pagination"];
            $scope.displayType = appData["p_listCss"]["Display type"];
            $scope.titleLength = appData["productCss"]["Title length"];
            $scope.refineText = appData["nlaCss"]["Text"];
            $scope.placeHolder = appData["searchCss"]["Placeholder"];
            $scope.footer = {css: getCssData(appConst.CONST.FOOTER.ID, appData["footerCss"])};
            $scope.nla = {css: getCssData(appConst.CONST.NLA.ID, appData["nlaCss"])};
            $scope.product = {css: getCssData(appConst.CONST.PRODUCT.ID, appData["productCss"])};
            $scope.body = {css: getCssData(appConst.CONST.BODY.ID, appData["bodyCss"])};
            $scope.header = {css: getCssData(appConst.CONST.HEADER.ID, appData["headerCss"])};
            $scope.logo = {css: getCssData(appConst.CONST.LOGO.ID, appData["logoCss"])};
            $scope.search = {css: getCssData(appConst.CONST.SEARCH.ID, appData["searchCss"])};
            $scope.category = {css: getCssData(appConst.CONST.CATEGORY.ID, appData["categoriesCss"])};
            $scope.pList = {css: getCssData(appConst.CONST.CATEGORY.ID, appData["p_listCss"])};
            $scope.products = getProducts($scope.totalCount, $scope.productCols, $scope.titleLength, $scope.product.css);
            $scope.loader = false;
            if (!$scope.$$phase)
                $scope.$digest();
//            saveToAppData($scope);
        });
        $scope.removeCat = function(){
            $scope.loader = true;
            messageService.sendMessage("updateCategoryInEditor", $scope.categories.splice(this.$index, 1)[0]);
            appConst.projectData["categoriesCss"]["categories"] = $scope.categories;
            $scope.loader = false;
        }
    });

    function getCssData(identifier, css){
        var appConst = AppConstants.getInstance();
        var style = {};
        for(var key in css){
            if(css.hasOwnProperty(key)){
                var lbl = appConst.CONST[identifier].CSS_NAMES[key.toLowerCase().replace(/\s/ig,"-")];
                if(!!lbl)
                    style[lbl] = css[key];
            }
        }
        return style;
    }

    function saveToAppData($scope){
        var appConst=AppConstants.getInstance();
        var appData=appConst.projectData;
        appData["bodyCss"]=getSaveCssObject(appData["bodyCss"],'BODY',$scope.body.css);
        appData["headerCss"]=getSaveCssObject(appData["headerCss"],'HEADER',$scope.header.css);
        appData["logoCss"]=getSaveCssObject(appData["logoCss"],'LOGO',$scope.logo.css);
        appData["searchCss"]=getSaveCssObject(appData["searchCss"],'SEARCH',$scope.search.css);
        appData["footerCss"]=getSaveCssObject(appData["footerCss"],'FOOTER',$scope.footer.css);
        appData["productCss"]=getSaveCssObject(appData["productCss"],'PRODUCT',$scope.product.css);
        appData["nlaCss"]=getSaveCssObject(appData["nlaCss"],'NLA',$scope.nla.css);
        appData["categoriesCss"]=getSaveCssObject(appData["categoriesCss"],'CATEGORY',$scope.category.css);
        appData["p_listCss"]=getSaveCssObject(appData["p_listCss"],'PLIST',$scope.pList.css);


        appData["categoriesCss"]["categories"] = $scope.categories;
        appData["p_listCss"]["Products Count"] = $scope.totalCount ;
        appData["p_listCss"]["Per Row Count"] = $scope.productCols ;
        appData["p_listCss"]["Pagination"] = $scope.showPagination;
        appData["p_listCss"]["Display type"] = $scope.displayType;
        appData["productCss"]["Title length"] = $scope.titleLength;
        appData["nlaCss"]["Text"] = $scope.refineText;
        appData["searchCss"]["Placeholder"] = $scope.placeHolder;
        /*var obj={};
        for(var key in appData["bodyCss"]){
            var lbl=key.toLowerCase().replace(/\s/ig,"-");
            obj[key]=$scope.body.css[appConst.CONST['BODY'].CSS_NAMES[lbl]];
        }*/


    }

    function getSaveCssObject(arr,identifier,cssObj){
        var obj={};
        var appConst = AppConstants.getInstance();
        for(var key in arr){
            if(arr.hasOwnProperty(key)){
                var value = cssObj[appConst.CONST[identifier].CSS_NAMES[key.toLowerCase().replace(/\s/ig,"-")]];
                if(value != null || value != undefined)
                    obj[key]= value;
            }
        }
        return obj;
    }

    function getProducts(totalCount, cols, titleLength, css, doNotUpdateCss){
        var width = {width: ((100 / cols) + "%")}, css = css || {};
        var products = [];
        for (var i = 0; i < totalCount; i++){
            var obj = {
                index: i,
                addClear: (i != 0 && (i % cols == 0)),
                title: AppConstants.getInstance().defaultTitle.substr(0,titleLength) + "...",
                style: width
            };
            if(!doNotUpdateCss){
                obj["imgStyle"] = css.img || "";
                obj["titleStyle"] = css.title || "";
                obj["priceStyle"] = css.price || "";
                obj["sellerStyle"] = css.sellerName || "";
                obj["ratingBlankStyle"] = css.ratingBlank || "";
            }
            products.push(obj);
        }
        return products;
    }
});

$().ready(function () {
    var _dashBoard = $(".dashboardContent");
    var _dashBoardPanel = $("#panel");
    var _loader = $("#dasboardLoader");
    var _leftNav = $(".leftNav");
    var _win = $(window);
    _win.resize(adjustDashBoard());
    function adjustDashBoard() {
        _dashBoard.height(_win.height() - 102);
        _leftNav.height(_win.height() - 102);
        return adjustDashBoard;
    }
    _win.load(function(){
        setTimeout(function(){
            _loader.hide();
            _dashBoardPanel.show();
        }, 2000);
    });
});
var adsWidget = angular.module("adsWidget", []);
adsWidget.directive("adunitList",function(){
    return{
        restrict:'E',
        replace: true,
        templateUrl:'/assets/angularTemplate/adunitList.html'
    }
});
adsWidget.directive("configurationForm",function(){
    return{
        restrict:'E',
        replace: true,
        templateUrl:'/assets/angularTemplate/configurationForm.html'
    }
});
adsWidget.directive("productDiv",function(){
    return{
        restrict:'E',
        replace: true,
        templateUrl:'/assets/angularTemplate/product.html'
    }
});
adsWidget.directive("accessManagementForm",function(){
    return{
        restrict:'E',
        replace: true,
        templateUrl:jsRoutes.controllers.adNetwork.dashboard.AccessManagementController.showAccessManage().url
    }
});
adsWidget.controller("loginCtrl", function ($scope) {
    $scope.showProfileOpts = function(){
        $scope.showOpts = !$scope.showOpts;
    }
});
adsWidget.controller("adsWidgetCtrl", function ($scope,$rootScope,adNetworkService,$http,$compile) {
	$scope.config = {};
	$scope.createAdUnit=function(){
		$scope.config = {};
		$scope.products=[];
		$('#view-adunit').hide();
		$rootScope.$broadcast('createAdunit');
	}
	$scope.viewAdUnit=function(){
		$('#create-adunit').hide();
		$scope.config = {};
		if($scope.adunits===undefined || $scope.isAdUnitDirty){
			var adunitlistpromise=adNetworkService.getAllAdunits(12321,"www.nextag.com");
			adunitlistpromise.then(function(data){
				$scope.adunits=data;
				if(!$scope.$$phase){
		            $scope.$digest();
		        }
			});
			$scope.isAdUnitDirty=false;
		}
		else{
			$('.adunitList').toggle();
		}
	}
	$scope.viewSelectedAdUnit=function($event){
		$event.stopPropagation();
		$('#create-adunit').hide();
	    $rootScope.$broadcast('viewAdunit',$($event.target).attr('id'));
	}
	$scope.getScript=function(){
		window.open(jsRoutes.controllers.adNetwork.dashboard.AdsDashBoardController.generatePdf().url,"_blank");
	}
	$scope.$watchCollection('[config.rowCount,config.columnCount]',function(newvalue){
    	if(newvalue!=undefined && newvalue[0]!=undefined && newvalue[1]!=undefined && newvalue[0].length>0 && newvalue[1].length){
    		var productPromise=adNetworkService.getProducts($scope.config.keyword,0,newvalue[0],newvalue[1]);
    		productPromise.then(function(data){
				$scope.products=data;	
			});
    	}
    });
    $scope.getCode=function(){
//        var url= jsRoutes.controllers.adNetwork.dashboard.AdsDashBoardController.getIframeCode(keyword,node,row,column).url;

        var src=encodeURI("http://172.23.98.251:9000/adsDashboard/getIframe/"+$scope.config.keyword+"/0/"+$scope.config.rowCount+"/"+$scope.config.columnCount);
		alert("<iframe src='"+src+"' width='100%' height='100%' />");
    }
    $scope.viewAccessManagement = function ($event) {
        console.log("in access management");
        $('#create-adunit,#view-adunit').hide();
        $('#panel').append($compile('<access-management-form></access-management-form>')($scope));

//        $("#dasboardLoader").show();
//        $http.get(jsRoutes.controllers.adNetwork.dashboard.AccessManagementController.showAccessManage().url, {})
//        .success(function (data) {
//            $("#panel").append(data);
//            console.log("success");
//        }).finally(function () {
//            $("#dasboardLoader").hide();
//        });
    }
});
adsWidget.factory('adNetworkService',['$http','$q',function($http,$q){
    var service={};
    service.createMerchantData=function(dataobj){
    	var deferred=$q.defer();
    	var url=jsRoutes.controllers.adNetwork.dashboard.MerchantDataController.insert().url;
    	console.log(dataobj);
    	$http.post(url,dataobj).success(function(data){
            console.log("success");
    		deferred.resolve();
        }).error(function(){
            deferred.reject();
        });
        return deferred.promise;
    }
    service.getAllAdunits=function(sellerId,domain){
    	var deferred=$q.defer();
    	var url=jsRoutes.controllers.adNetwork.dashboard.MerchantDataController.getAllAdunits(sellerId,domain).url;
    	console.log(url);
    	$http.get(url).success(function(data){
            console.log("success");
    		deferred.resolve(data);
        }).error(function(){
            deferred.reject();
        });
        return deferred.promise;
    }
    service.getProducts=function(keyword,node,row,column){
    	var deferred=$q.defer();
    	var url= jsRoutes.controllers.adNetwork.dashboard.AdsDashBoardController.getProducts(keyword,node,row,column).url;
    	console.log(url);
    	$http.get(url).success(function(data){
            console.log("success");
    		deferred.resolve(data);
        }).error(function(){
            deferred.reject();
        });
        return deferred.promise;
    }
return service;
}
]);
App.onAppLoad(function(ngApp){
    ngApp.directive('adunitList',function(){
	    return{
	        restrict:'E',
	        scope:{
	        	datasource:"=",
	        	action:"&"
	        },
	        replace: true,
	        templateUrl:jsRoutes.controllers.Assets.at('angularTemplate/adNtwrk/adunitList.tmp.html').url,
	        link: function (scope, element) {
	        	scope.selectedindex=0;
	        	element.bind('click',function(event){
	        		event.stopPropagation();
	        		var aditem=$(event.target);
	        		var selectedid=aditem.attr('id');
	        		scope.selectedindex=selectedid;
	        		scope.action()(aditem.data('action'),selectedid);
	        	});
	        }
	    };
	});
});

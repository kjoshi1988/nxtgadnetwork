App.onAppLoad(function (ngApp) {
    ngApp.directive('productDiv', function ($timeout) {
        return{
            restrict: 'E',
            templateUrl: jsRoutes.controllers.Assets.at('angularTemplate/adNtwrk/product.tmp.html').url
        };
    });
});

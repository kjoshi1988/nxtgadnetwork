App.onAppLoad(function (ngApp) {
    ngApp.directive("slideToggle", function ($http, messageService) {
        return {
            restrict:'E',
            replace: true,
            scope:{
                slideModel : "=",
                defaultText : "@",
                toggleText: "@",
                toggleAction:"&"
            },
            templateUrl:jsRoutes.controllers.Assets.at('angularTemplate/adNtwrk/slideToggle.tmp.html').url,
            link: function ($scope, element, attrs) {
                element.bind('click',function(e){
                    var _targetClass = e.target.className;
                    if (!!_targetClass && _targetClass.indexOf("editText") > -1)  {
                        $scope.slideModel = !$scope.slideModel;
                        if(!$scope.$$phase)
                            $scope.$apply();
                        $scope.toggleAction()($scope.slideModel,$scope);
                    }
                });
            }
        }
    });
});

App.onAppLoad(function (ngApp) {
    ngApp.directive('configurationForm', function () {
        var _lastTimeOut;
        return{
            restrict: 'E',
            replace: true,
            templateUrl: jsRoutes.controllers.Assets.at('angularTemplate/adNtwrk/configurationForm.tmp.html').url,
            link: function ($scope, ielm) {
                $scope.$watch('[source.adUnit.rows,source.adUnit.columns,source.pricelow,source.pricehigh,source.adUnit.node,source.adUnit.keyword]', function (newvalue, oldvalue) {
                    !!_lastTimeOut && clearTimeout(_lastTimeOut);
                    if (!!$scope.source.adUnit.rows && !!$scope.source.adUnit.columns && !!$scope.source.adUnit.keyword && $scope.source.adUnit.keyword.trim().length >= 2)
                        _lastTimeOut = setTimeout(function(){
                            $scope.source.updateProducts(getQueryString($scope), $scope.source.adUnit.rows, $scope.source.adUnit.columns, $scope);
                        }, $scope.source.editMode ? 400: 0);
                });
                function getQueryString(scope) {
                    var queryString = "search=" + scope.source.adUnit.keyword + "&perpage=" + scope.source.adUnit.rows * scope.source.adUnit.columns;
                    if (scope.source.adUnit.node != "" && scope.source.adUnit.node != undefined)
                        queryString += "&node=" + scope.source.adUnit.node;
                    if (!!scope.source.pricelow)
                        queryString += "&plw=" + scope.source.pricelow;
                    if (!!scope.source.pricehigh)
                        queryString += "&phi=" + scope.source.pricehigh;
                    //$scope.nodename = $("#node option:selected").text();
                    return queryString;
                }
            }
        };
    });
});

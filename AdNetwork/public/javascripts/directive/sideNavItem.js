App.onAppLoad(function(ngApp){
    ngApp.directive("sideNavItem", function(templateService){
        return {
            restrict:'A',
            replace: true,
            link: function ($scope, element) {
                element.bind('click',function(e){
                    templateService.addTemplate.call(this,this.getAttribute("data-viewAction"), $scope);
                    e.cancelBubble = true;
                    e.stopPropagation();
                    e.stopImmediatePropagation();
                    return false;
                });
            }
        }

    })
});

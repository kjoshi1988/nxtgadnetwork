App.onAppLoad(function (ngApp) {
    ngApp.directive("tabbedRadio", function ($http, messageService) {
        return {
            restrict:'E',
            replace: true,
            scope:{
                itemModel : "=",
                itemList : "=",
                disabled : "=",
                label: "@",
                value: "@"
            },
            templateUrl:jsRoutes.controllers.Assets.at('angularTemplate/adNtwrk/tabbedRadio.tmp.html').url,
            link: function ($scope, element, attrs) {
                if(!$scope.label) $scope.label = "label";
                if(!$scope.value) $scope.value = "label";
                if(!!$scope.itemList && $scope.itemList.length > 0){
                    if(!$scope.itemModel)
                        $scope.itemModel = $scope.itemList[0][$scope.value];
                    else {
                        var _hasModel = false;
                        for (var i = 0; i < $scope.itemList.length; i++) {
                            if($scope.itemList[i] === $scope.itemModel){
                                _hasModel = true;
                                break;
                            }
                        }
                        if(!_hasModel)
                            $scope.itemModel = $scope.itemList[0][$scope.value];
                    }
                }
                element.bind('click',function(e){
                    if(!$scope.disabled){
                        $scope.itemModel = $scope.itemList[parseInt(e.target.getAttribute("data-index"))][$scope.value];
                        if(!$scope.$$phase)
                            $scope.$apply();
                    }
                });
            }
        }
    });
});
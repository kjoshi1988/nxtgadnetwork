App.onAppLoad(function(ngApp){
    ngApp.directive('leftnavOption',function(){
	    return{
	        restrict:'A',
	        replace: true,
	        scope:{
	        	action:"&"
	        },
	        link: function (scope, element) {
	        	element.bind('click',function(e){
	        		scope.selecteditem=$(this).data('viewaction');
	        		scope.selectedEvent=$(this).data('event');
	        		if(scope.selectedEvent===undefined)
	        			scope.action()(scope.selecteditem);
	        		else
	        			scope.action()(scope.selectedEvent,scope.selecteditem);
	        	});
	        }
	    };
	});
});

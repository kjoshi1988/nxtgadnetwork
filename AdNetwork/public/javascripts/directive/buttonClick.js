App.onAppLoad(function(ngApp){
    ngApp.directive('buttonClick',function(){
	    return{
	        restrict:'E',
	        templateUrl:jsRoutes.controllers.Assets.at('angularTemplate/adNtwrk/getButton.tmp.html').url,
	        link: function ($scope, element) {
	        	element.bind('click',function(e){
	        		switch($scope.source.adUnit.modeType){
	        		case(AppConstants.getInstance().modetypes[0].label):
	        			var src=AppConstants.getInstance().getBaseurl()+AppConstants.getInstance().getIframeUrl($scope.source.adUnit.adUnitId);
	        			alert("<iframe src='"+src+"' width='100%' height='100%' />");
	        			break;
	        		case(AppConstants.getInstance().modetypes[1].label):
	        			window.open(AppConstants.getInstance().getPdfUrl($scope.source.adUnit.adUnitId),"_blank");
	        		}
	        	});
	        }
	    };
	});
});

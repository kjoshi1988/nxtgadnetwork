App.onAppLoad(function(ngApp){
    ngApp.directive("rightView",function(adNetworkService, templateService){
        return{
            restrict: 'E',
            replace: true,
            scope:{
                "source":"=",
                "template":"@"
            },
            template: '<div ng-include="templateUrl"></div>',
            link: function ($scope, ele) {
                var appConst = AppConstants.getInstance();
                var tmpId = $scope["template"];
                $scope.templateUrl = appConst.CONSTANTS[tmpId].templateUrl;
            }
        }
    });
});

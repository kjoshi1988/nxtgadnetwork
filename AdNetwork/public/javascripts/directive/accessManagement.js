App.onAppLoad(function(ngApp){
    ngApp.directive("accessManagementForm",function(){
        return{
            restrict:'E',
            replace: true,
            templateUrl:jsRoutes.controllers.adNetwork.dashboard.AccessManagementController.showAccessManage().url
        }
    });
});
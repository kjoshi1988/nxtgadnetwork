$().ready(function(){
    $("#loginForm").submit(function(){
        $.ajax({
            url: "/auth/validate",
            type: "POST",
            data: {uname: this.uname.value, pwd: this.password.value},
            success: function (resp) {
                if(resp == "true")
                    window.location.href = "/";
            },
            error: function () {
            }
        });
        return false;
    });
});
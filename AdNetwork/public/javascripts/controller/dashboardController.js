App.onAppLoad(function (ngApp) {
    function fetchCategories(categories) {
        var catArr = [];
        for (var key in categories) {
            if (categories.hasOwnProperty(key)) {
                var cat = categories[key];
                var catObj = {label: cat["name"], value: cat["id"]};
                if (!cat["children"])
                    catObj["children"] = fetchCategories(cat["children"]);
                catArr.push(catObj);
            }
        }
        return catArr;
    }

    ngApp.controller('adsWidgetCtrl', function (messageService, $scope, $compile, adNetworkService,$timeout) {
        $scope.isSubCategoryVisible = false;
        $scope.category = -1;
        $scope.subCategory = -1;
        $scope.categoryName="";
        $scope.arr = {};
        $scope.config = {};
        $scope.arr.adunits = [];
        $scope.adtypes = AppConstants.getInstance().adtypes;
        $scope.modetypes = AppConstants.getInstance().modetypes;
        $scope.layoutType = AppConstants.getInstance().layoutType;
        var categories = AppConstants.getInstance().categories;
        if (!!categories)
            $scope.arr.categories = categories;
        else {
            adNetworkService.getData(AppConstants.getInstance().defaultCategoryUrl).then(function (data) {
                $scope.arr.categories = data;
                AppConstants.getInstance().categories = data;
                if (!$scope.$$phase)
                    $scope.$digest();
            });
        }
        $scope.isAdUnitDirty = true;
        $scope.arr.products = [];
        angular.element(document).ready(function(){
        	$scope.sendMessage('VIEW_AD_UNITS');
        });
        $scope.sendMessage = function (message, data) {
            messageService.sendMessage(message, data);
        }
        $scope.updateProducts = function (queryString, rows, columns) {
        	$scope.arr.products=[];
        		$('#productLoader').show();
            var productPromise = adNetworkService.getData(AppConstants.getInstance().getProductsUrl(queryString, rows, columns));
            productPromise.then(function (data) {
                $scope.arr.products = data;
                $('#productLoader').hide();
                if (!$scope.$$phase)
                    $scope.$digest();
            });
        }
        $scope.$on( "ACCESS_MANAGEMENT" , function ($event) {
            console.log("in access management");
            $('#create-adunit,#view-adunit').hide();
            var accessManagePanel = document.getElementById("accessManagementView");
            if(!accessManagePanel)
                $('#panel').append($compile('<access-management-form></access-management-form>')($scope));
            else $(accessManagePanel).show();
        });
        $scope.selectSubCategory = function(){
            $scope.isSubCategoryVisible = false;
            $scope.arr.subCatData = $scope.arr.categories[$scope.config.node]["children"];
        };
        $scope.$on('CREATE_NEW_AD', function (event) {
            $scope.config = {};
            $scope.arr.products = [];

            AppConstants.getInstance().$viewUnit.hide();
            AppConstants.getInstance().$createUnit.show();
            $scope.isEditMode = true;
            if (AppConstants.getInstance().$createUnit.html().trim().length == 0) {
                AppConstants.getInstance().$createUnit.append($compile('<configuration-form ></configuration-form><product-div></product-div>')($scope));
            }
            if (!$scope.$$phase)
                $scope.$digest();
        });
        $scope.sendData = function () {
            $scope.config.nlaTypes = [];
            var priceObj={};
            priceObj.nlaName="$"+$scope.pricelow+" - $"+$scope.pricehigh;
            priceObj.nlaValue=[];
            if(!!$scope.pricelow)
                priceObj.nlaValue.push("plw="+$scope.pricelow);
            if(!!$scope.pricehigh)
                priceObj.nlaValue.push("phi="+$scope.pricehigh);
            priceObj.nlaValue = priceObj.nlaValue.join("&");
            $scope.config.nlaTypes.push(priceObj);
            if($scope.config.adUnitId==undefined){
            	$scope.config.adUnitId=0;
            	var createPromise=adNetworkService.sendData(AppConstants.getInstance().createMerchantDataUrl(), $scope.config);
            	createPromise.then(function(data){
            		$scope.config.adUnitId=data.adUnitId;
            		$scope.isEditMode = false;
            		if (!$scope.$$phase) {
                $scope.$digest();
            }
            	});
            }
            else{
            	adNetworkService.updateData(AppConstants.getInstance().updateMerchantDataUrl(), $scope.config);
            }

            $scope.isAdUnitDirty = true;
        };
        $scope.$on('VIEW_SELECTED_ADUNIT', function (event, id) {
            if (AppConstants.getInstance().$viewUnit.html().trim().length == 0) {
                AppConstants.getInstance().$viewUnit.append($compile('<configuration-form></configuration-form><product-div></product-div>')($scope));
                $scope.isEditMode = false;
            }
            AppConstants.getInstance().currentSlectedAdUnit=id;
            $.extend($scope.config, $scope.arr.adunits[id]);
            var categoryObj=$scope.config.node && $scope.arr.categories[$scope.config.node];
            if($scope.config.nlaTypes.length>0){
            	$scope.pricelow=parseInt($scope.config.nlaTypes[0].nlaValue.split('&')[0].split('=')[1]);
            $scope.pricehigh=parseInt($scope.config.nlaTypes[0].nlaValue.split('&')[1].split('=')[1]);
            }
            $scope.nodename=categoryObj!=undefined?categoryObj.name:"";
            if (!$scope.$$phase) {
                $scope.$digest();
            }
            AppConstants.getInstance().$viewUnit.show();
            AppConstants.getInstance().$createUnit.hide();
        });
        $scope.$on('VIEW_AD_UNITS', function (event, id) {
            AppConstants.getInstance().$createUnit.hide();
            $scope.config = {};
            if ($scope.isAdUnitDirty) {
            		 $("#dasboardLoader").show();
                var promise = adNetworkService.getData(AppConstants.getInstance().getAllAdUnitsUrl(12345678, "www.nextag.com"));
                promise.then(function (data) {
                    //$.extend($scope.adunits,data);
                    $scope.arr.adunits = data;
                    if(AppConstants.getInstance().currentSlectedAdUnit==undefined)
                    	$scope.sendMessage('VIEW_SELECTED_ADUNIT', 0);
                    $("#dasboardLoader").hide();
                    if (!$scope.$$phase) {
                        $scope.$digest();
                    }
                });
                $scope.isAdUnitDirty = false;
            }
            else {
                $('.adunitList').toggle();
            }
        });
    });
});

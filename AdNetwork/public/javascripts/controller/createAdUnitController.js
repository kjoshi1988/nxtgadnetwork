App.onAppLoad(function(ngApp){
    ngApp.controller('createAdUnitCtrl',function(messageService,$scope,adNetworkService,$compile){
		$scope.$on('CREATE_NEW_AD', function (event) {
			$scope.config = {};
			$scope.config.nlaTypes={a:[0]};
			$scope.arr.products=[];
			AppConstants.getInstance().$viewUnit.hide();
			AppConstants.getInstance().$createUnit.show();
			$scope.isEditMode=true;
			if(AppConstants.getInstance().$createUnit.html().trim().length==0){
				AppConstants.getInstance().$createUnit.append($compile('<configuration-form ></configuration-form><product-div></product-div>')($scope));
			}
			$scope.adtypes=AppConstants.getInstance().adtypes;
	    	$scope.modetypes=AppConstants.getInstance().modetypes;
	    	$scope.layoutType=AppConstants.getInstance().layoutType;
	    	if(!$scope.$$phase)
    			$scope.$digest();
		});
		$scope.sendData=function(){
	    	$scope.config.adUnitId=0;
	    	adNetworkService.sendData(AppConstants.getInstance().createMerchantDataUrl(),$scope.config);
	    	$scope.$parent.isAdUnitDirty=true;
	    	$scope.isEditMode=false;
	    };
	});
});

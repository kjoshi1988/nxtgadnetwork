App.onAppLoad(function (ngApp) {
    function fetchCategories(categories) {
        var catArr = [];
        for (var key in categories) {
            if (categories.hasOwnProperty(key)) {
                var cat = categories[key];
                var catObj = {
                    label: cat["name"],
                    value: cat["id"]
                };
                if (!cat["children"])
                    catObj["children"] = fetchCategories(cat["children"]);
                catArr.push(catObj);
            }
        }
        return catArr;
    }

    function initSource() {
        var sourceObj = {};
        var appConst = AppConstants.getInstance();
        sourceObj[appConst.CONSTANTS.VIEW_AD_UNITS.ID] = {};
        sourceObj[appConst.CONSTANTS.CREATE_NEW_AD.ID] = {};
        return sourceObj;
    }

    ngApp.controller('adsWidgetCtrl', function (templateService, messageService, $scope, $compile, adNetworkService, $timeout) {
        var appConst = AppConstants.getInstance();
        $scope.source = initSource();
        $scope.init = function () {
            templateService.registerRootElements(document.getElementById("panel"), document.getElementById("dashboardLoader"));
            adNetworkService.getData(AppConstants.getInstance().defaultCategoryUrl).then(function (data) {
                $scope.source[appConst.CONSTANTS.VIEW_AD_UNITS.ID].categories = data;
                $scope.source[appConst.CONSTANTS.CREATE_NEW_AD.ID].categories = data;
            });
            templateService.addTmpShowEvent(appConst.CONSTANTS.VIEW_AD_UNITS.ID, function () {
                templateService.showLoader();
                var source = $scope.source[appConst.CONSTANTS.VIEW_AD_UNITS.ID];
                source.currentIndex = 0;
                source.editMode = false;
                source.adUnit = {};
                source.modeList = appConst.modetypes;
                source.adTypeList = appConst.adtypes;
                source.layoutTypeList = appConst.layoutTypes;
                if ($.isEmptyObject(source.adUnits)) {
                    var promise = adNetworkService.getData(AppConstants.getInstance().getAllAdUnitsUrl(12345678, "www.nextag.com"));
                    promise.then(function (data) {
                        templateService.showLoader();
                        if(!data || data.length == 0) {
                            source.adUnits = [{"displayText": "No data to show"}];
                            source._noData = true;
                        } else
                            source.adUnits = data;
                        source.rowSelection(0);
                        templateService.hideLoader();
                    });
                } else {
                    source.rowSelection(0);
                    templateService.hideLoader();
                }
            });
            templateService.addTmpShowEvent(appConst.CONSTANTS.CREATE_NEW_AD.ID, function () {
                templateService.showLoader();
                var source = $scope.source[appConst.CONSTANTS.CREATE_NEW_AD.ID];
                source.adUnit = {};
                source.editMode = true;
                source.adTypeList = appConst.adtypes;
                source.layoutTypeList = appConst.layoutTypes;
                source.modeList = appConst.modetypes;
                source.products = [];
                if(!$scope.$$phase)
                    $scope.$digest();
                templateService.hideLoader();
            });

            templateService.addTemplate(appConst.CONSTANTS.VIEW_AD_UNITS.ID, $scope);
        };
        $scope.source[appConst.CONSTANTS.CREATE_NEW_AD.ID].updateProducts = updateProducts;
        $scope.source[appConst.CONSTANTS.VIEW_AD_UNITS.ID].updateProducts = updateProducts;
        $scope.source[appConst.CONSTANTS.CREATE_NEW_AD.ID].editSaveAction = function (editMode, scope) {
            var source = $scope.source[appConst.CONSTANTS.CREATE_NEW_AD.ID];
            if(!editMode){
                if (!source.adUnit.displayText || source.adUnit.displayText.trim().length == 0) {
                    source.editMode = true;
                    if(!$scope.$$phase)
                        $scope.$digest();
                    alert("Please provide a heading");
                } else if(!source.adUnit.keyword || source.adUnit.keyword.trim().length == 0) {
                    source.editMode = true;
                    if(!scope.$$phase)
                        scope.$digest();
                    alert("Please provide a keyword");
                } else if(source.adUnit.rows <= 0) {
                    source.editMode = true;
                    if(!scope.$$phase)
                        scope.$digest();
                    alert("Please provide a valid row count");
                } else if(source.adUnit.columns <= 0) {
                    source.editMode = true;
                    if(!scope.$$phase)
                        scope.$digest();
                    alert("Please provide a valid column count");
                } else {
                    source.adUnit.nlaTypes = [];
                    var priceObj = {};
                    priceObj.nlaName = "$" + source.pricelow + " - $" + source.pricehigh;
                    priceObj.nlaValue = [];
                    if (!!source.pricelow)
                        priceObj.nlaValue.push("plw=" + source.pricelow);
                    if (!!source.pricehigh)
                        priceObj.nlaValue.push("phi=" + source.pricehigh);
                    priceObj.nlaValue = priceObj.nlaValue.join("&");
                    source.adUnit.nlaTypes.push(priceObj);
                    source.adUnit.adUnitId = 0;
                    var createPromise = adNetworkService.sendData(AppConstants.getInstance().createMerchantDataUrl(), source.adUnit);
                    createPromise.then(function (data) {
                        source.adUnit.adUnitId = data.adUnitId;
                        $scope.source[appConst.CONSTANTS.VIEW_AD_UNITS.ID].adUnits.push(source.adUnit);
                    });
                }
            }
        };
        $scope.source[appConst.CONSTANTS.VIEW_AD_UNITS.ID].rowSelection = function (index) {
            var source = $scope.source[appConst.CONSTANTS.VIEW_AD_UNITS.ID];
            if(!source._noData){
                source.adUnit = cloneAdUnit(this["adUnits"][index]);
                source.editMode = false;
                source.currentIndex = index;
                if (!source.adUnit.backgroundColor)
                    source.adUnit.backgroundColor = "#ffffff";
                if (source.adUnit.nlaTypes.length > 0) {
                    if (source.adUnit.nlaTypes[0].nlaValue.match(/plw=([0-9]*)/) != null) {
                        source.pricelow = parseInt(source.adUnit.nlaTypes[0].nlaValue.match(/plw=([0-9]*)/)[1]);
                    }
                    if (source.adUnit.nlaTypes[0].nlaValue.match(/phi=([0-9]*)/) != null) {
                        source.pricehigh = parseInt(source.adUnit.nlaTypes[0].nlaValue.match(/phi=([0-9]*)/)[1]);
                    }
                }
            }
        };
        $scope.source[appConst.CONSTANTS.VIEW_AD_UNITS.ID].deleteAdUnit = function (event, index) {
            var source = $scope.source[appConst.CONSTANTS.VIEW_AD_UNITS.ID];
            if(!source._noData){
                var _confirm = window.confirm("Do you want to delete this ad unit?");
                if (_confirm) {
                    adNetworkService.deleteData(AppConstants.getInstance().deleteMerchantDataUrl(source.adUnits[index].adUnitId));
                    source.adUnits.splice(index, 1);
                }
            }
        };
        $scope.source[appConst.CONSTANTS.VIEW_AD_UNITS.ID].editSaveAction = function (editMode) {
            if (!editMode) {
                var source = $scope.source[appConst.CONSTANTS.VIEW_AD_UNITS.ID];
                if(!source._noData){
                    source.adUnits[source.currentIndex] = source.adUnit;
                    source.adUnit = source.adUnits[source.currentIndex];
                    source.adUnit.nlaTypes = [];
                    var priceObj = {};
                    priceObj.nlaName = "$" + source.pricelow + " - $" + source.pricehigh;
                    priceObj.nlaValue = [];
                    if (!!source.pricelow)
                        priceObj.nlaValue.push("plw=" + source.pricelow);
                    if (!!source.pricehigh)
                        priceObj.nlaValue.push("phi=" + source.pricehigh);
                    priceObj.nlaValue = priceObj.nlaValue.join("&");
                    source.adUnit.nlaTypes.push(priceObj);
                    adNetworkService.updateData(AppConstants.getInstance().updateMerchantDataUrl(), source.adUnit);
                }
            }
        };
        function cloneAdUnit(obj) {
            return JSON.parse(JSON.stringify(obj));
        }

        function updateProducts(queryString, rows, columns, scope) {
            scope.fetchingProducts = true;
            scope.noProductsToShow = false;
            var productPromise = adNetworkService.getData(AppConstants.getInstance().getProductsUrl(queryString, rows, columns));
            productPromise.then(function (data) {
                if(!data || !data["0"])
                    scope.noProductsToShow = true;
                else {
                    scope.noProductsToShow = false;
                    scope.source.products = data;
                }
                scope.fetchingProducts = false;
            });
        }
    });
});

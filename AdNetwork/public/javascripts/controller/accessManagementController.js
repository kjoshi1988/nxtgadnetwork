App.onAppLoad(function (ngApp) {
    ngApp.controller('accessManageViewCtrl', function ($scope, adNetworkService) {
        $scope.sellerId = $("#accessManageSellerId").val();
        $scope.accessToken = $("#accessToken").val();
        $scope.generateNewAccessToken = function () {
            var promise = adNetworkService.sendData(AppConstants.getInstance().generateAccessTokenUrl(), {sellerId: $scope.sellerId});
            promise.then(function (data) {
                $scope.accessToken = data;
                if (!$scope.$$phase) {
                    $scope.$digest();
                }
            });
        }
    });
});
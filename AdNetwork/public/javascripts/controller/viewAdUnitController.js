App.onAppLoad(function(ngApp){
    ngApp.controller('viewAdUnitCtrl',function(messageService,$scope,adNetworkService,$compile){
		$scope.$on('VIEW_SELECTED_ADUNIT', function (event,id) {
			if(AppConstants.getInstance().$viewUnit.html().trim().length==0){
				AppConstants.getInstance().$viewUnit.append($compile('<configuration-form></configuration-form><product-div></product-div>')($scope));
				$scope.isEditMode=false;
			}
			$.extend($scope.config,$scope.arr.adunits[id]);
			if(!$scope.$$phase){
	            $scope.$digest();
	        }
			AppConstants.getInstance().$viewUnit.show();
			AppConstants.getInstance().$createUnit.hide();
		});
		$scope.$on('VIEW_AD_UNITS', function (event,id) {
			AppConstants.getInstance().$createUnit.hide();
			$scope.config = {};
			if($scope.$parent.isAdUnitDirty){
				var promise=adNetworkService.getData(AppConstants.getInstance().getAllAdUnitsUrl(123456,"www.nextag.com"));
				promise.then(function(data){
					//$.extend($scope.adunits,data);
					$scope.arr.adunits=data;
					if(!$scope.$$phase){
			            $scope.$digest();
			        }
				});
				$scope.$parent.isAdUnitDirty=false;
			}
			else{
				$('.adunitList').toggle();
			}
		});
    });
});

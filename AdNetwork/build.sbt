name := """AdNetwork"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  javaWs
)

libraryDependencies ++= Seq(
  "commons-lang" % "commons-lang" % "2.3",
  "org.json" % "json" % "20140107",
  "org.springframework" % "spring-context-support" % "3.2.2.RELEASE",
  "org.apache.thrift" % "libthrift" % "0.9.2",
  "com.google.code.gson" % "gson" % "2.3" % "provided",
  "commons-io" % "commons-io" % "2.4",
  "mysql" % "mysql-connector-java" % "5.1.6",
  "com.itextpdf" % "itextpdf" % "5.2.1"
)



includeFilter in (Assets, LessKeys.less) := "*.less"

excludeFilter in (Assets, LessKeys.less) := "_*.less"
